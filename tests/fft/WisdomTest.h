/// @file
/// 
/// @brief Tests of the wisdom files and related functionality
/// @details The tests of the singleton itself are not very useful but can 
/// reveal internal inconsistencies. We also exercise here reading and writing
/// of wisdom files. As these tests overwrite wisdom files, the setup temporary 
/// replaces FFTW_WISDOM environment variable with the value of FFTW_WISDOM_TEST
/// variable. If the latter doesn't exist, the current directory "./" is assumed as
/// a place to store wisdom files. The original value of FFTW_WISDOM variable is
/// restored at the end.
///
/// @copyright (c) 2022 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>

#ifndef SCIMATH_FFT_WISDOM_TEST
#define SCIMATH_FFT_WISDOM_TEST

#include <cppunit/extensions/HelperMacros.h>

//#include <askap/scimath/fft/FFTWrapperTypeTraits.h>
#include <askap/scimath/fft/WisdomSingleton.h>
#include <casacore/casa/BasicSL/Complex.h>


#include <string>
#include <boost/shared_ptr.hpp>

#include <casacore/casa/OS/EnvVar.h>
#include <casacore/casa/OS/Path.h>
#include <casacore/casa/OS/RegularFile.h>

// lofar (for OpenMP wrapper)
#include <Common/OpenMP.h>

namespace askap {

namespace scimath {

class WisdomTest : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(WisdomTest);
  CPPUNIT_TEST(testSingleton);
  CPPUNIT_TEST(testWisdomGeneration);
  CPPUNIT_TEST_SUITE_END();
private:
  /// @brief helper class to manage FFTW_WISDOM
  /// It saves the value in the constructor and restores in the destructor
  class EnvVarHelper {
  public:
     /// @brief constructor - caches the value of FFTW_WISDOM, if defined
     EnvVarHelper() : itsVarName("FFTW_WISDOM") { 
         if (casacore::EnvironmentVariable::isDefined(itsVarName)) {
             itsOldValue = get();
         }
     }
     /// @brief method to set FFTW_WISDOM
     /// @param[in] val new value
     void set(const std::string &val) const {
        casacore::EnvironmentVariable::set(itsVarName, val);
     }

     /// @brief short cut to get the current value of FFTW_WISDOM (we use it in tests too)
     std::string get() const {
        CPPUNIT_ASSERT(casacore::EnvironmentVariable::isDefined(itsVarName));
        return casacore::EnvironmentVariable::get(itsVarName);
     }

     /// @brief destructor, restores old value cached in constructor
     ~EnvVarHelper() {
        set(itsOldValue);
     }
  private:
     /// @brief value of managed environment variable
     const std::string itsVarName;
     /// @brief old value
     std::string itsOldValue;
  };

  /// @brief shared pointer to EnvVarHelper instance 
  boost::shared_ptr<EnvVarHelper> itsEnvVarHelper;

  public:

  void setUp() {
    itsEnvVarHelper.reset(new EnvVarHelper());
    const std::string testWisdomPathVar = "FFTW_WISDOM_TEST";
    if (casacore::EnvironmentVariable::isDefined(testWisdomPathVar)) {
        itsEnvVarHelper->set(casacore::EnvironmentVariable::get(testWisdomPathVar));
    } else {
        itsEnvVarHelper->set(casacore::Path("./").absoluteName());
    }
  }
  
  void tearDown() {
    // this will reset the original value of FFTW_TEST
    itsEnvVarHelper.reset();
  }

  void testSingleton() {
     // singleton is not a very unit-testable concept, but should be no harm exercising it here
     // One of the challenges with testing a singleton is that other tests can affect this one. 
     // Therefore, we use the friendship between the test and singleton classes to ensure we start from scratch
     // (although this essentially means that the singleton is not really a singleton, but it is ok given our particular
     // implementation 
     
     WisdomSingleton::theirInstance.reset();
     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(0u), WisdomSingleton::getInstance().itsNumThreadsForWisdom.size());
     CPPUNIT_ASSERT(WisdomSingleton::theirInstance);
     // make a copy of the internal shared pointer to verify that it indeed behaves as a singleton at the end
     boost::shared_ptr<WisdomSingleton> wsCopy = WisdomSingleton::theirInstance;
     CPPUNIT_ASSERT(itsEnvVarHelper);
     CPPUNIT_ASSERT_EQUAL(itsEnvVarHelper->get(), WisdomSingleton::getInstance().itsWisdomDirectory);
     // another ways to get wisdom directory, just for the sake of the test (the first is equivalent to data member access, 
     // the second one repeats the whole job of getting the value instead of returning the cached value
     CPPUNIT_ASSERT_EQUAL(itsEnvVarHelper->get(), WisdomSingleton::getInstance().wisdomDirectory());
     CPPUNIT_ASSERT_EQUAL(itsEnvVarHelper->get(), WisdomSingleton::getInstance().obtainWisdomDirectory());
     CPPUNIT_ASSERT(WisdomSingleton::getInstance().wisdomConfigured());
     testWisdomFileName("fftw-double-3threads.wisdom", WisdomSingleton::getInstance().wisdomFileName<casacore::DComplex>(3));
     testWisdomFileName("fftw-float-5threads.wisdom", WisdomSingleton::getInstance().wisdomFileName<casacore::Complex>(5));
     // nothing still should be cached because we didn't exercise on-demand reading of wisdom files 
     // (it is better to do as part of the wisdom generation test as we cannot be assured that some wisdom files are not
     // present on the disk as a left over from the previous test runs)
     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(0u), WisdomSingleton::getInstance().itsNumThreadsForWisdom.size());
     
     // check that we still have the original object, so it is actually a singleton
     CPPUNIT_ASSERT_EQUAL(wsCopy, WisdomSingleton::theirInstance);
     wsCopy.reset();
     // and dispose the singleton itself (although this way it won't actually be a singleton, technically)
     WisdomSingleton::theirInstance.reset();
  }

  void testWisdomGeneration() {
     // as before, reset the internal shared pointer to ensure that the new instance of the singleton has been created
     // (just for the test)
     WisdomSingleton::theirInstance.reset();
     WisdomSingleton& ws = WisdomSingleton::getInstance();
     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(0u), ws.itsNumThreadsForWisdom.size());

     // it is difficult to ensure that this test is performed with the given number of threads as we may not even have multithreading compiled in
     // However, it should work anyway, just the number of threads would be 1 in this case.
     const int numThreads = getNumThreads();
     const int size = 5120;

     // generateWisdom reads wisdom file if available. To ensure a fresh start for unit tests, delete the wisdom files first if they are present
     
     removeFileIfPresent(ws.wisdomFileName<casacore::Complex>(numThreads));
     removeFileIfPresent(ws.wisdomFileName<casacore::DComplex>(numThreads));

     {
        FFT2DWrapper<casacore::Complex> wrapper(false, numThreads);
        wrapper.generateWisdom(size,size, numThreads, FFTW_MEASURE);
        // do some FFT which should load wisdom file (either with the given number of threads or for the single thread case)
        casacore::Matrix<casacore::Complex> buf(size, size, casacore::Complex(1.f));
        wrapper(buf, true);
     }
     // one element should be in the cache now (for one run of the FFT)
     const std::map<int, int>& cache = WisdomSingleton::getInstance().itsNumThreadsForWisdom;
     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(1u), cache.size());
     {
        FFT2DWrapper<casacore::DComplex> wrapper(false, numThreads);
        wrapper.generateWisdom(size,size, numThreads, FFTW_MEASURE);
        // do some FFT which should load wisdom file (either with the given number of threads or for the single thread case)
        casacore::Matrix<casacore::DComplex> buf(size, size, casacore::DComplex(1.));
        wrapper(buf, true);
     }
     // two elements should be in the cache now
     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(2u), cache.size());

     // check that the number of threads for which the wisdom has been cached is numThreads
     for (std::map<int,int>::const_iterator ci = cache.begin(); ci != cache.end(); ++ci) {
          CPPUNIT_ASSERT_EQUAL(numThreads, ci->second);
     }
  }
  private:
     /// @brief helper method to do checks for the paths
     /// @details It verifies both path component and the file name
     /// @param[in] expected expected file name without the full path
     /// @param[in] name2test file name to test (with full path)
     void testWisdomFileName(const std::string &expectedName, const std::string &name2test) {
         const casacore::Path path(name2test);
         // casacore's String is now derived from std::string, so do static cast as opposed to copy
         CPPUNIT_ASSERT_EQUAL(itsEnvVarHelper->get(), static_cast<std::string>(path.dirName()));
         CPPUNIT_ASSERT_EQUAL(expectedName, static_cast<std::string>(path.baseName()));
     }

     /// @brief helper method to remove the file, if present
     /// @details Nothing is done if the file doesn't exist.
     /// @param[in] name file name to remove (full path expected)
     void removeFileIfPresent(const std::string &name) {
        casacore::RegularFile file(name);
        if (file.exists()) {
            file.remove();
        }
     }

     /// @brief helper method to get the number of threads used for the test
     /// @details It is handy to restrict the number of threads to a smaller value than 
     /// potentially is available. Besides, we also need to return 1 if the library is
     /// compiled without OpenMP support (and note, this could just be no OpenMP in FFTW, 
     /// and not in the rest of the library)
     /// @note it is handy to have a separate method similar to the one available in the wrapper, so
     /// the content of the cache can be checked. Given the logic in getMaxNumThreads, the actual number of
     /// threads used in the plan will always correspond to that returned by this method if this number is passed
     /// to the wrapper at construction.
     /// @return the number of threads to use for the test (cast to signed int given the rest of the interface)
     int getNumThreads() const {
        #ifdef HAVE_FFTW_OPENMP
        const int maxNumThreads = static_cast<int>(LOFAR::OpenMP::maxThreads());
        return maxNumThreads > 4 ? 4 : maxNumThreads;
        #else
        return 1;
        #endif
     }
};

} // namespace scimath

} // namespace askap

#endif // #define SCIMATH_FFT_WISDOM_TEST
