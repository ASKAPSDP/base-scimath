/// @file ffttest.cc
///
/// @brief Simple test of FFTW in parallel with OpenMP
/// @details It is designed to be sufficiently simple so one could build it both in
/// our build system and in a standalone way. It was instrumental for performance tests
/// (see AXA-1770 for details) and investigation of tricky cases when linking to an unexpected
/// specialised (but not well-suitable) library occurred. It takes no parameters and reports
/// some performance metrics (and details on how it was compiled) into the standard output. 
/// Note, define HAVE_FFTW_OPENMP compiler variable to include the code specific to 
/// OpenMP-enabled FFTW (otherwise, it would be effectively serial even if linked against
/// *_omp libraries).
///
/// @copyright (c) 2007 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>

#include <iostream>
#include <ctime>
#include <vector>
#include <complex>

#include <fftw3.h>

// we could've used LOFAR::OpenMP::maxThreads here instead of explicit calls to the omp.h method
// however, doing it explicitly allows easy compilation of this test application outside of
// our build system as it doesn't depend on any of our repos
#ifdef _OPENMP
#include <omp.h>
#endif 

#include <sys/types.h>
#include <sys/times.h>
#include <unistd.h>


class Timer {
  public:
    Timer() : itsStart(getTime()) {}
    double real() const { return static_cast<double>(getTime() - itsStart) / ((double)sysconf(_SC_CLK_TCK)); }
    static clock_t getTime() { tms usage; return times(&usage);}
    void mark() { itsStart = getTime();}
  private:
    clock_t itsStart;
};

template<typename T>
T square(T in) { return in * in;}

template<typename T>
void addGaussian(std::vector<std::complex<T> > &buffer, size_t sz, T peak, T row_off, T col_off, T row_width, T col_width)
{
  std::complex<T> *data = buffer.data();
  #pragma omp parallel for 
  for (int row = 0; row < sz; ++row) {
       const T part = square((row - static_cast<T>(sz) / 2 - static_cast<T>(row_off)) / row_width);
       for (int col = 0; col < sz; ++col) {
            const T rad2 =  part + square((col - static_cast<T>(sz) / 2 - static_cast<T>(col_off)) / col_width);
            data[row*sz+col] += peak * exp(-4.*log(2.) * rad2);
       }
  }
}

void fillBuffer(std::vector<std::complex<float> > &buf, size_t sz)
{
  buf.resize(sz*sz, std::complex<float>(0.f,0.f));
  addGaussian(buf, sz, 1.f, 10.5f, -13.1f, 4.0f, 5.5f); 
}

void doTest(std::vector<std::complex<float> > &buf, size_t sz)
{
   fftwf_complex* buffer = reinterpret_cast<fftwf_complex*>(buf.data());

   #ifdef HAVE_FFTW_OPENMP
   fftwf_plan_with_nthreads(omp_get_max_threads());
   #endif

   #ifdef _OPENMP
   std::cout<<"current max number of OMP threads: "<<omp_get_max_threads()<<std::endl;
   #endif
 
   fftwf_plan p = fftwf_plan_dft_2d(sz, sz, buffer, buffer, FFTW_FORWARD, FFTW_ESTIMATE);
   Timer timer;
   
   fftwf_execute_dft(p, buffer, buffer);
   std::cout<<"transform time: "<<timer.real()<<std::endl;

   fftwf_destroy_plan(p);
}

int main() {
  #ifdef _OPENMP
  std::cout<<"built with OpenMP support"<<std::endl;
  #else 
  std::cout<<"built without OpenMP support"<<std::endl;
  #endif
  Timer timer;
  const size_t size = 12880;
  std::vector<std::complex<float> > buffer;
  fillBuffer(buffer, size);
  #ifdef HAVE_FFTW_OPENMP
  std::cout<<"built with FFTW OpenMP libraries"<<std::endl;
  std::cout<<"running init_threads, it returned: "<<fftwf_init_threads()<<std::endl;
  #else
  std::cout<<"built without FFTW OpenMP libraries, skip fftwf_init_threads() call as it may not be present"<<std::endl;
  #endif
  
  std::cout<<"initialisation time: "<<timer.real()<<std::endl;
  doTest(buffer, size);
  std::cout<<"total run time: "<<timer.real()<<std::endl;
  return 0;
}
