/// @file
/// 
/// @brief Tests of multithreading initialisation logic
/// @details These tests are not very useful but can reveal internal inconsistencies 
/// and/or crashes in some circumstances. Essentially a no operation if the code
/// is built without multithreading support. Initialisation itself doesn't require
/// miltiple threads, it just calls required methods of FFTW.
///
/// @copyright (c) 2022 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>

#ifndef SCIMATH_FFT_THREAD_INIT_TEST
#define SCIMATH_FFT_THREAD_INIT_TEST

#include <cppunit/extensions/HelperMacros.h>

#include <askap/scimath/fft/FFTWrapperTypeTraits.h>
#include <askap/scimath/fft/ThreadInitSingleton.h>
#include <casacore/casa/BasicSL/Complex.h>

#include <vector>

namespace askap {

namespace scimath {

class ThreadInitTest : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(ThreadInitTest);
  CPPUNIT_TEST(testSinglePrecisionInit);
  CPPUNIT_TEST(testDoublePrecisionInit);
  CPPUNIT_TEST(testSeveralInits);
  CPPUNIT_TEST(testSingleton);
  CPPUNIT_TEST_SUITE_END();
private:
  /// @brief effectively a convenient typedef
  template<typename FT>
  using GuardType = typename FFTWrapperTypeTraits<FT>::ThreadInitGuard;

  // helper class to test that constructor/destructor is called
  struct Counter : public ThreadInitGuardBase {
     /// @brief constructor, remembers the reference to destruction counter, incremenets construction counter
     Counter(unsigned int &nConstruction, unsigned int &nDestruction) : itsDestructionCounter(nDestruction) { ++nConstruction; }
     /// @brief destructor, increments stored destruction counter
     virtual ~Counter() { ++itsDestructionCounter; }
  private:
     // counter of the number of destructions
     unsigned int &itsDestructionCounter;
  };

  /// @brief test of the single precision initialiser
  void testSinglePrecisionInit() {
     GuardType<casacore::Complex> guard;
  }

  /// @brief test of the double precision initialiser
  void testDoublePrecisionInit() {
     GuardType<casacore::DComplex> guard;
  }

  /// @brief test of various initialisers in a sequence
  void testSeveralInits() {
     // simple initialisation-deinitialisation
     { 
        GuardType<casacore::Complex> guard;
     }
     {
        GuardType<casacore::DComplex> guard;
     }
     // same in sequence with the overlap
     std::vector<boost::shared_ptr<ThreadInitGuardBase> > initGuards(2);
     initGuards[0].reset(new GuardType<casacore::Complex>());
     initGuards[1].reset(new GuardType<casacore::DComplex>());
     initGuards[0].reset();
     initGuards[1].reset();
     // and in reversed order
     initGuards[0].reset(new GuardType<casacore::DComplex>());
     initGuards[1].reset(new GuardType<casacore::Complex>());
     initGuards[0].reset();
     initGuards[1].reset();
     // using a fake test guard to verify constructor and destructor are indeed called
     unsigned int nConstruction = 0u, nDestruction = 0u;
     initGuards[0].reset(new Counter(nConstruction, nDestruction));
     CPPUNIT_ASSERT_EQUAL(1u, nConstruction);
     CPPUNIT_ASSERT_EQUAL(0u, nDestruction);
     initGuards[1].reset(new Counter(nConstruction, nDestruction));
     CPPUNIT_ASSERT_EQUAL(2u, nConstruction);
     CPPUNIT_ASSERT_EQUAL(0u, nDestruction);
     initGuards[0].reset();
     CPPUNIT_ASSERT_EQUAL(2u, nConstruction);
     CPPUNIT_ASSERT_EQUAL(1u, nDestruction);
     initGuards[1].reset();
     CPPUNIT_ASSERT_EQUAL(2u, nConstruction);
     CPPUNIT_ASSERT_EQUAL(2u, nDestruction);
  }

  void testSingleton() {
     // singleton is not a very unit-testable concept, but should be no harm exercising it here
     // One of the challenges with testing a singleton is that other tests can affect this one. 
     // Therefore, we use the friendship between the test and singleton classes to ensure we start from scratch
     // (although this essentially means that the singleton is not really a singleton, but it is ok given our particular
     // implementation and the fact that FFTW doesn't mind to be initialised/deinitialised multiple times by the looks of it(
     ThreadInitSingleton::theirInstance.reset();
     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(0u), ThreadInitSingleton::getInstance().itsInitGuards.size());

     ThreadInitSingleton::getInstance().init<casacore::Complex>();
     // we can use the friendship relation between test class and the singleton to verify uniqueness
     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(1u), ThreadInitSingleton::getInstance().itsInitGuards.size());
     ThreadInitSingleton::getInstance().init<casacore::DComplex>();
     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(2u), ThreadInitSingleton::getInstance().itsInitGuards.size());
     // the size should remain the same after the following line (already initialised)
     ThreadInitSingleton::getInstance().init<casacore::Complex>();
     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(2u), ThreadInitSingleton::getInstance().itsInitGuards.size());

     // use frendship between test class and the singleton to force resource deallocation,
     // quite deliberately there are no public methods for it
     ThreadInitSingleton::getInstance().itsInitGuards.clear();
     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(0u), ThreadInitSingleton::getInstance().itsInitGuards.size());
     // and dispose the singleton itself (although this way it won't actually be a singleton, technically)
     ThreadInitSingleton::theirInstance.reset();
  }
};

} // namespace scimath

} // namespace askap

#endif // #ifndef SCIMATH_FFT_THREAD_INIT_TEST
