/// @copyright (c) 2007 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///

// ASKAPsoft includes
#include <askap/askap/AskapTestRunner.h>
#include <askap/askap/AskapLogging.h>


// Test includes
#include <FFTTest.h>
#include <FFT2DTest.h>
#include <ThreadInitTest.h>
#include <WisdomTest.h>
#include <FFTUtilsTest.h>

// casa includes
#include <casacore/casa/OS/File.h>

// std includes
#include <string>

ASKAP_LOGGER(logger, "scimath.fft");

int main(int argc, char *argv[])
{
    // We use logging in various areas in FFTW wrapper.
    // initialise the logger to be able to see the messages
    // (although the tests themselves would work without)
    // It may be worth to overhaul logging initialisation at some
    // point - currently I (MV) not sure the code below helps much,
    // but log messages appear. 
    const std::string defaultLogConfig("askap.log_cfg");
    const casacore::File logConfigFile(defaultLogConfig); 
    if (logConfigFile.exists()) {
        ASKAPLOG_INIT(defaultLogConfig.c_str());
    } else {
        const std::string specialLogConfig = std::string(argv[0]) + ".log_cfg";
        ASKAPLOG_INIT(specialLogConfig.c_str());
    }

    askapdev::testutils::AskapTestRunner runner(argv[0]);
    runner.addTest(askap::scimath::FFTTest::suite());
    runner.addTest(askap::scimath::FFT2DTest::suite());
    runner.addTest(askap::scimath::ThreadInitTest::suite());
    runner.addTest(askap::scimath::WisdomTest::suite());
    runner.addTest(askap::scimath::FFTUtilsTest::suite());
    bool wasSucessful = runner.run();

    return wasSucessful ? 0 : 1;
}
