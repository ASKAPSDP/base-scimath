/// @file
/// 
/// @brief Tests of the 2D FFT and wrappers
///
/// @copyright (c) 2022 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>

#ifndef SCIMATH_2DFFT_TEST
#define SCIMATH_2DFFT_TEST

#include <cppunit/extensions/HelperMacros.h>

#include <askap/askap/AskapError.h>

#include <casacore/casa/BasicSL/Constants.h>
#include <fftw3.h>
#include <casacore/casa/Arrays/Vector.h>
#include <casacore/casa/Arrays/Matrix.h>
#include <askap/scimath/fft/FFTWrapper.h>
#include <askap/scimath/fft/FFTWrapperTypeTraits.h>
#include <askap/scimath/fft/FFT2DWrapper.h>

#include <casacore/casa/Arrays/ArrayMath.h>
#include <askap/scimath/utils/OptimizedArrayMathUtils.h>

// for debugging
#include <askap/scimath/utils/ImageUtils.h>
#include <casacore/casa/OS/Timer.h>

// FFTW - include directly to be able to test plan creation 
#include <fftw3.h>

// boost include
#include <boost/shared_array.hpp>

namespace askap {

namespace scimath {

class FFT2DTest : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(FFT2DTest);
  CPPUNIT_TEST(testShiftSinglePrecision);
  CPPUNIT_TEST(testShiftDoublePrecision);
  CPPUNIT_TEST(testShiftSinglePrecision2);
  CPPUNIT_TEST(testShiftDoublePrecision2);
  CPPUNIT_TEST(testConjSinglePrecision);
  CPPUNIT_TEST(testConjDoublePrecision);
  CPPUNIT_TEST(testConjOddSizeSinglePrecision);
  CPPUNIT_TEST(testConjOddSizeDoublePrecision);
  CPPUNIT_TEST(testTypeAssumptions);
  CPPUNIT_TEST(testSharedPlanPointers);
  CPPUNIT_TEST(testSignFlips);
  CPPUNIT_TEST(testFFTShift);
  CPPUNIT_TEST(testFFTShift2);
  CPPUNIT_TEST(testNewWrapperSinglePrecision);
  CPPUNIT_TEST(testNewWrapperDoublePrecision);
  CPPUNIT_TEST(testNewWrapperOddSizeSinglePrecision);
  CPPUNIT_TEST(testSineWave);
  CPPUNIT_TEST(testArrayTransform<float>);
  CPPUNIT_TEST(testArrayTransform<double>);
  //the following test is used for experiments, it doesn't test anything new and, therefore, has been commented out
  //CPPUNIT_TEST(testExperiments);
  CPPUNIT_TEST_SUITE_END();

protected:

  /// @brief check that the buffer contains a Gaussian
  /// @details This method is similar to utility::addGaussian, but instead of adding it checks the buffer content
  /// to match the values if a single addGaussian call would have been issued for an empty buffer
  /// @param[in] buffer the buffer to work with
  /// @param[in] peak amplitude of the peak
  /// @param[in] row_off offset in the first coordinate in pixels
  /// @param[in] col_off offset in the second coordinate in pixels
  /// @param[in] row_width width along the first coordinate in pixels
  /// @param[in] col_width width along the second coordinate in pixels
  template <typename T, typename FT>
  static void checkGaussian(const Matrix<FT> &buffer, T peak, T row_off, T col_off, T row_width, T col_width);

  /// @brief apply phase gradient in the Fourier plane
  /// @details This will cause a shift in the image plane
  /// @param[in] buffer the buffer to work with
  /// @param[in] row_off offset in the first coordinate (i.e l)
  /// @param[in] col_off offset in the second coordinate (i.e. m)
  template <typename T, typename FT>
  static void applyPhaseGradient(Matrix<FT> &buffer, T row_off, T col_off);

  /// @brief check that the value matches expectations
  /// @details Having this templated method enables application to both
  /// real and complex inputs. If the input is complex, it is assumed that
  /// the imaginary part should be zero.
  /// @param[in] expected expected value (real)
  /// @param[in] measured measured value (real or complex)
  template <typename T, typename FT>
  static void checkValue(T expected, FT measured);

  template <typename T>
  static void checkValue(T expected, T measured);

  /// @brief helper method to obtain comparison tolerance
  /// @return tolerance to be used for floating point comparison (type-dependent)
  template <typename T>
  static T getTolerance();

  /// @brief basic test employing the shift property of the FT
  /// @details It simulates an offset Gaussian, does FT, applies phase
  /// gradient to shift it back to the origin, FT back and verifies the result.
  /// This is a reasonably thorough test ensuring the order of harminics is right
  /// @param[in] size_x size of the grid on the first axis
  /// @param[in] size_y size of the grid on the second axis
  /// @param[in] width_x width of the gaussian in pixels the first axis
  /// @param[in] width_y width of the gaussian in pixels the second axis
  template<typename T>
  static void testShift(casacore::uInt size_x, casacore::uInt size_y, T width_x, T width_y);

  /// @brief test of conjugation property for real input
  /// @details It is one of the best ways to verify that harmonics are placed where we expect them.
  /// Three Gaussian sources are simulated, one at the origin and two with some offsets.
  /// @param[in] size size of the grid to work with (same for both axes)
  /// @param[in] width width of the Gaussian in pixels (same for both axes and all sources)
  template<typename T>
  static void testConjugation(casacore::uInt size, T width);

  /// @brief test new wrapper
  /// @details This test does forward and reverse FFT for an offset gaussian and compares results 
  /// against the old (generic) wrapper. This test should pick up differences in harmonic placement,
  /// scaling, etc.
  /// @param[in] size_x size of the grid on the first axis
  /// @param[in] size_y size of the grid on the second axis
  /// @param[in] width_x width of the gaussian in pixels the first axis
  /// @param[in] width_y width of the gaussian in pixels the second axis
  template<typename T>
  static void testNewWrapper(casacore::uInt size_x, casacore::uInt size_y, T width_x, T width_y);

  /// @brief test with simple sine wave
  /// @details Generates a sine wave, does FFT, checks the results + same with inverse FFT
  /// @param[in] wrapper wrapper to work with, sharing it between multiple calls enables to test caching as well
  /// @param[in] size_x size of the grid on the first axis
  /// @param[in] size_y size of the grid on the second axis
  /// @param[in] freq_x frequency of the sine wave on the first axis (in pixels after FFT)
  /// @param[in] freq_y frequency of the sine wave on the second axis (in pixels after FFT)
  template<typename FT>
  static void testNewWrapperSineWave(FFT2DWrapper<FT> &wrapper, casacore::uInt size_x, casacore::uInt size_y, casacore::uInt freq_x, casacore::uInt freq_y);

  /// @brief naive unoptimised implementation of the FFT shift via direct data copy and buffering
  /// @details This method has originally been debugged as part of the wrapper and moved here to be used to test optimised 
  /// implementation provided by the wrapper now
  /// @param[in] data array with data to modify in situ
  /// @param[in] afterFFT a flag of the direction of the shift; true if it corresponds to the call after FFT
  template<typename FT>
  void fftShiftNaive(casacore::Matrix<FT> &data, bool afterFFT);

  /// @brief perform the test of FFT shift routine for the given size and data type
  /// @details This method is called from testFFTShift2 a number of times for the different array sizes and data types
  /// (as optimised implementation may behave differently with even and odd grid sizes as well as for complex and double complex grids)
  /// @param[in] size_x number of pixels on the first axis
  /// @param[in] size_y number of pixels on the second axis
  template<typename FT>
  void testFFTShiftVsNaiveImplementation(casacore::uInt size_x, casacore::uInt size_y); 

  /// @brief perform the test of array transform for the given data type (template parameter)
  template<typename T>
  void testArrayTransform();
public:
  /// @brief single precision test via shift
  void testShiftSinglePrecision();

  /// @brief double precision test via shift
  void testShiftDoublePrecision();

  /// @brief single precision test via shift for rectangular image and elliptical Gaussian
  void testShiftSinglePrecision2();

  /// @brief double precision test via shift for rectangular image and elliptical Gaussian
  void testShiftDoublePrecision2();

  /// @brief single precision test of conjugation property
  /// @details Essentially it verifies that harmonic placement is as expected
  void testConjSinglePrecision();

  /// @brief double precision test of conjugation property
  /// @details Essentially it verifies that harmonic placement is as expected
  void testConjDoublePrecision();

  /// @brief single precision test of conjugation property for an odd image size
  /// @details Essentially it verifies that harmonic placement is as expected
  void testConjOddSizeSinglePrecision();

  /// @brief double precision test of conjugation property for an odd image size
  /// @details Essentially it verifies that harmonic placement is as expected
  void testConjOddSizeDoublePrecision();

  /// @brief test assumptions about FFTW types/their match to casacore types
  void testTypeAssumptions();

  /// @brief test of handling shared pointers to a plan
  void testSharedPlanPointers();

  /// @brief test new wrapper against the old one in single precision
  void testNewWrapperSinglePrecision();

  /// @brief test new wrapper against the old one in double precision
  void testNewWrapperDoublePrecision();

  /// @brief test new wrapper against the old one for odd number of pixels, single precision
  void testNewWrapperOddSizeSinglePrecision();

  /// @brief test helper routine to ensure correct harmonic placement
  void testSignFlips();

  /// @brief test helper routine to ensure correct harmonic placement via direct data move
  /// @details This method tests against some predefined matrix with known result
  void testFFTShift();

  /// @brief test fft shift method of harmonic rearrangement
  /// @details unlike testFFTShift it compares against naive unoptimised implementation provided
  /// by the protected fftShiftNaive method of this class
  void testFFTShift2();

  /// @brief test with simple sine wave
  void testSineWave();

  // temporary
  void testExperiments();
};

// we could've just used std::complex<T>, but this way doesn't rely
// on the fact that casacore complex type is the same as STL's
template<typename T>
struct FTTypeHelper {};

template<>
struct FTTypeHelper<casacore::Float> {
   typedef casacore::Complex FT;
};

template<>
struct FTTypeHelper<casacore::Double> {
   typedef casacore::DComplex FT;
};


/// @brief check that the value matches expectations
/// @details Having this templated method enables application to both
/// real and complex inputs. If the input is complex, it is assumed that
/// the imaginary part should be zero.
/// @param[in] expected expected value (real)
/// @param[in] measured measured value (real or complex)
template <typename T, typename FT>
void FFT2DTest::checkValue(T expected, FT measured)
{
  CPPUNIT_ASSERT_DOUBLES_EQUAL(expected, casacore::real(measured), getTolerance<T>());
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0., casacore::imag(measured), getTolerance<T>());
}


template <typename T>
void FFT2DTest::checkValue(T expected, T measured)
{
  CPPUNIT_ASSERT_DOUBLES_EQUAL(expected, measured, getTolerance<T>());
}

/// @brief helper method to obtain comparison tolerance
/// @return tolerance to be used for floating point comparison (type-dependent)
template <typename T>
T FFT2DTest::getTolerance() 
{
  return static_cast<T>(1e-12);
}

template <>
float FFT2DTest::getTolerance<float>() 
{
  return 3e-7f;
}

/// @brief check that the buffer contains a Gaussian
/// @details This method is similar to utility::addGaussian, but instead of adding it checks the buffer content
/// to match the values if a single addGaussian call would have been issued for an empty buffer
/// @param[in] buffer the buffer to work with
/// @param[in] peak amplitude of the peak
/// @param[in] row_off offset in the first coordinate in pixels
/// @param[in] col_off offset in the second coordinate in pixels
/// @param[in] row_width width along the first coordinate in pixels
/// @param[in] col_width width along the second coordinate in pixels
template <typename T, typename FT>
void FFT2DTest::checkGaussian(const Matrix<FT> &buffer, T peak, T row_off, T col_off, T row_width, T col_width)
{
  ASKAPASSERT(buffer.nelements() > 0);
  for (Int row = 0; row < buffer.nrow(); ++row) {
       const T part = casacore::square((row - static_cast<T>(buffer.nrow()) / 2 - static_cast<T>(row_off)) / row_width);
       for (Int col = 0; col < buffer.ncolumn(); ++col) {
            const T rad2 =  part + square((col - static_cast<T>(buffer.ncolumn()) / 2 - static_cast<T>(col_off)) / col_width);
            const T expected = peak * exp(-4.*log(2.) * rad2);
            const FT measured = buffer(row,col);
            checkValue(expected, measured);
        }
  }
}

/// @brief apply phase gradient in the Fourier plane
/// @details This will cause a shift in the image plane
/// @param[in] buffer the buffer to work with
/// @param[in] row_off offset in the first coordinate (i.e l)
/// @param[in] col_off offset in the second coordinate (i.e. m)
template <typename T, typename FT>
void FFT2DTest::applyPhaseGradient(Matrix<FT> &buffer, T row_off, T col_off)
{
  ASKAPASSERT(buffer.nelements() > 0);
  const T cell_u = 1. / static_cast<T>(buffer.nrow());
  const T cell_v = 1. / static_cast<T>(buffer.ncolumn());
  // for odd-sized buffer the centre of FFT is at pixel = int(size)/2
  const int centreRow = static_cast<int>(buffer.nrow()) / 2;
  const int centreCol = static_cast<int>(buffer.ncolumn()) / 2;
  for (Int row = 0; row < buffer.nrow(); ++row) {
       const T u = cell_u * (row - static_cast<T>(centreRow));
       for (Int col = 0; col < buffer.ncolumn(); ++col) {
            const T v = cell_v * (col - static_cast<T>(centreCol));
            const T phasor = -2.*casacore::C::pi * (u * row_off + v * col_off);
            buffer(row,col) *= FT(cos(phasor), sin(phasor));
       }
  }
}

/// @brief basic test employing the shift property of the FT
/// @details It simulates an offset Gaussian, does FT, applies phase
/// gradient to shift it back to the origin, FT back and verifies the result.
/// This is a reasonably thorough test ensuring the order of harminics is right
/// @param[in] size_x size of the grid on the first axis
/// @param[in] size_y size of the grid on the second axis
/// @param[in] width_x width of the gaussian in pixels the first axis
/// @param[in] width_y width of the gaussian in pixels the second axis
template<typename T>
void FFT2DTest::testShift(casacore::uInt size_x, casacore::uInt size_y, T width_x, T width_y)
{
  const T off_row = -10;
  const T off_col = 5;

  casacore::Matrix<typename FTTypeHelper<T>::FT> buffer(size_x, size_y, 0.);
  utility::addGaussian(buffer, static_cast<T>(1.), off_row, off_col, width_x, width_y);
  //saveAsCasaImage("tst.img", casacore::real(buffer));
  fft2d(buffer, true);
  applyPhaseGradient(buffer, -off_row, -off_col);
  fft2d(buffer, false);
  checkGaussian(buffer, static_cast<T>(1.), static_cast<T>(0.), static_cast<T>(0.), width_x, width_y);
  //saveAsCasaImage("tst.img", casacore::amplitude(buffer));
  // this check is redundant, it just tests working with real grid via templates
  const Matrix<T> realPart = casacore::real(buffer);
  checkGaussian(realPart, static_cast<T>(1.), static_cast<T>(0.), static_cast<T>(0.), width_x, width_y);
}

/// @brief test helper routine to ensure correct harmonic placement
void FFT2DTest::testSignFlips()
{
   casacore::Matrix<casacore::Complex> tmp(10,5,casacore::Complex(1.,1.));
   FFT2DWrapper<casacore::Complex>::flipSigns(tmp.shape(), boost::shared_ptr<float>(reinterpret_cast<float*>(tmp.data()), utility::NullDeleter()));
   for (casacore::uInt row = 0; row < tmp.nrow(); ++row) {
        for (casacore::uInt col = 0; col < tmp.ncolumn(); ++col) {
             const float expected = (col + row) % 2  == 0 ? 1. : -1.;
             checkValue(expected, casacore::real(tmp(row,col)));
             checkValue(expected, casacore::imag(tmp(row,col)));
        }
   }
}

template<typename FT>
void FFT2DTest::fftShiftNaive(casacore::Matrix<FT> &data, bool afterFFT)
{
   const casacore::IPosition shape = data.shape();
   ASKAPDEBUGASSERT(shape.size() == 2);
   ASKAPDEBUGASSERT(shape[0] > 0);
   ASKAPDEBUGASSERT(shape[1] > 0);

   const casacore::Int xshift = afterFFT ? shape[0] / 2 : (shape[0] + 1) / 2;
   const casacore::Int yshift = afterFFT ? shape[1] / 2 : (shape[1] + 1) / 2;

   // naive unoptimised implementation via simple copy
   typename casacore::Matrix<FT> copy(data.copy());
   for (casacore::Int i = 0; i < shape[0]; ++i) {
        const casacore::Int ii = (i + xshift) % shape[0];
        for (casacore::Int j = 0; j < shape[1]; ++j) {
             const casacore::Int jj = (j + yshift) % shape[1];
             data(ii,jj) = copy(i,j);
        }
   }
}

/// @brief test helper routine to ensure correct harmonic placement via direct data move
/// @details This method tests against some predefined matrix with known result
void FFT2DTest::testFFTShift()
{
   casacore::Matrix<casacore::Complex> tmp(3,3,casacore::Complex(0.,-1.));
   for (casacore::uInt row = 0, counter = 1; row < tmp.nrow(); ++row) {
        for (casacore::uInt col = 0; col < tmp.ncolumn(); ++col, ++counter) {
             tmp(row, col) = casacore::Complex(static_cast<float>(counter),0.f);
        }
   }
   casacore::Matrix<casacore::Complex> tmp2 = tmp.copy();
   FFT2DWrapper<casacore::Complex>::fftShift(tmp, true);
   FFT2DWrapper<casacore::Complex>::fftShift(tmp2, false);

   const int expected1[9] = {9, 7, 8, 3, 1, 2, 6, 4, 5};
   const int expected2[9] = {5, 6, 4, 8, 9, 7, 2, 3, 1};
   for (casacore::uInt row = 0, counter = 0; row < tmp.nrow(); ++row) {
        for (casacore::uInt col = 0; col < tmp.ncolumn(); ++col, ++counter) {
             CPPUNIT_ASSERT(counter < 9);
             checkValue(static_cast<float>(expected1[counter]), tmp(row,col));
             checkValue(static_cast<float>(expected2[counter]), tmp2(row,col));
        }
   }
}

/// @brief perform the test of FFT shift routine for the given size and data type
/// @details This method is called from testFFTShift2 a number of times for the different array sizes and data types
/// (as optimised implementation may behave differently with even and odd grid sizes as well as for complex and double complex grids)
/// @param[in] size_x number of pixels on the first axis
/// @param[in] size_y number of pixels on the second axis
template<typename FT>
void FFT2DTest::testFFTShiftVsNaiveImplementation(casacore::uInt size_x, casacore::uInt size_y) 
{
   casacore::Matrix<FT> tmp(size_x,size_y, static_cast<FT>(0.));
   for (casacore::uInt row = 0; row < size_x; ++row) {
        for (casacore::uInt col = 0; col < size_y; ++col) {
             tmp(row, col) = FT(row * size_y + col, col * size_x + row);
        }
   }
   casacore::Matrix<FT> copy(tmp.copy());
   // shift from the wrapper
   FFT2DWrapper<FT>::fftShift(tmp, true);
   // shift via naive algorithm
   fftShiftNaive<FT>(copy, true);
   // compare results
   for (casacore::uInt row = 0; row < tmp.nrow(); ++row) {
        for (casacore::uInt col = 0; col < tmp.ncolumn(); ++col) {
             const FT difference = copy(row, col) - tmp(row, col);
             checkValue(static_cast<typename FT::value_type>(0.), difference);
        }
   }

   // shift back
   FFT2DWrapper<FT>::fftShift(tmp, false);
   fftShiftNaive<FT>(copy, false);

   // compare results, both between two implementations and against the expected values
   for (casacore::uInt row = 0; row < tmp.nrow(); ++row) {
        for (casacore::uInt col = 0; col < tmp.ncolumn(); ++col) {
             const FT difference = copy(row, col) - tmp(row, col);
             checkValue(static_cast<typename FT::value_type>(0.), difference);
             checkValue(static_cast<typename FT::value_type>(row * size_y + col), real(copy(row, col)));
             checkValue(static_cast<typename FT::value_type>(col * size_x + row), imag(copy(row, col)));
        }
   }
}

/// @brief test fft shift method of harmonic rearrangement
/// @details unlike testFFTShift it compares against naive unoptimised implementation provided
/// by the protected fftShiftNaive method of this class
void FFT2DTest::testFFTShift2()
{
   // single precision complex versions
   testFFTShiftVsNaiveImplementation<casacore::Complex>(127,127);
   testFFTShiftVsNaiveImplementation<casacore::Complex>(64,64);
   testFFTShiftVsNaiveImplementation<casacore::Complex>(255,64);
   testFFTShiftVsNaiveImplementation<casacore::Complex>(32,127);
   // double precision complex versions
   testFFTShiftVsNaiveImplementation<casacore::DComplex>(127,127);
   testFFTShiftVsNaiveImplementation<casacore::DComplex>(64,64);
   testFFTShiftVsNaiveImplementation<casacore::DComplex>(255,64);
   testFFTShiftVsNaiveImplementation<casacore::DComplex>(32,127);
}

/// @brief test new wrapper
/// @details This test does forward and reverse FFT for an offset gaussian and compares results 
/// against the old (generic) wrapper. This test should pick up differences in harmonic placement,
/// scaling, etc.
/// @param[in] size_x size of the grid on the first axis
/// @param[in] size_y size of the grid on the second axis
/// @param[in] width_x width of the gaussian in pixels the first axis
/// @param[in] width_y width of the gaussian in pixels the second axis
template<typename T>
void FFT2DTest::testNewWrapper(casacore::uInt size_x, casacore::uInt size_y, T width_x, T width_y)
{
  const T off_row = -10;
  const T off_col = 5;

  casacore::Matrix<typename FTTypeHelper<T>::FT> buffer(size_x, size_y, 0.);
  utility::addGaussian(buffer, static_cast<T>(1.), off_row, off_col, width_x, width_y);
  utility::addGaussian(buffer, static_cast<T>(0.5), off_row*2, off_col*0.5f, width_x, width_y);
  casacore::Matrix<typename FTTypeHelper<T>::FT> buffer2(buffer.copy());
  CPPUNIT_ASSERT_EQUAL(buffer.nrow(), buffer2.nrow());
  CPPUNIT_ASSERT_EQUAL(buffer.ncolumn(), buffer2.ncolumn());
  fft2d(buffer, true);
  //saveAsCasaImage("tst.img", casacore::real(buffer));
  FFT2DWrapper<typename FTTypeHelper<T>::FT> newWrapper;
  newWrapper(buffer2, true);
 
  for (casacore::uInt row = 0; row < buffer.nrow(); ++row) {
       for (casacore::uInt col = 0; col < buffer.ncolumn(); ++col) {
            // our CI hosts show different numerical noise for some reason to the production hosts - need a bit relaxed tolerance
            CPPUNIT_ASSERT_DOUBLES_EQUAL(casacore::real(buffer(row,col)), casacore::real(buffer2(row,col)), getTolerance<T>()*size_x*size_y);
            CPPUNIT_ASSERT_DOUBLES_EQUAL(casacore::imag(buffer(row,col)), casacore::imag(buffer2(row,col)), getTolerance<T>()*size_x*size_y);
       }
  }
  // now FFT back
  fft2d(buffer, false);
  newWrapper(buffer2, false);

  for (casacore::uInt row = 0; row < buffer.nrow(); ++row) {
       for (casacore::uInt col = 0; col < buffer.ncolumn(); ++col) {
            // our CI hosts show different numerical noise for some reason to the production hosts - need a bit relaxed tolerance
            CPPUNIT_ASSERT_DOUBLES_EQUAL(casacore::real(buffer(row,col)), casacore::real(buffer2(row,col)), getTolerance<T>()*(size_x+size_y)/2);
            CPPUNIT_ASSERT_DOUBLES_EQUAL(casacore::imag(buffer(row,col)), casacore::imag(buffer2(row,col)), getTolerance<T>()*(size_x+size_y)/2);
       }
  }

}


/// @brief perform the test of array transform for the given data type (template parameter)
template<typename T>
void FFT2DTest::testArrayTransform()
{
   const casacore::IPosition shape(4,128,168,2,3);
   casacore::Array<typename FTTypeHelper<T>::FT> buffer(shape, static_cast<typename FTTypeHelper<T>::FT>(0.));

   const T off_row = -10;
   const T off_col = 5;
   for (int x = 0; x < shape[2]; ++x) {
        for (int y = 0; y < shape[3]; ++y) {
            const casacore::IPosition blc(4, 0, 0, x, y);
            const casacore::IPosition trc(4, shape[0] - 1, shape[1] - 1, x, y);
            // take a slice (i.e. reference semantics), deliberately use a different way to get the slice from the implementation to double-check it
            Matrix<typename FTTypeHelper<T>::FT> slice = buffer(blc,trc).nonDegenerate(2);
            utility::addGaussian(slice, static_cast<T>(x * y) / 10.f, off_row, off_col, static_cast<T>(8.) + x, static_cast<T>(9.) + y);
        }
   }
   FFT2DWrapper<typename FTTypeHelper<T>::FT> wrapper;
   wrapper.transformAllHyperPlanes(buffer, true);
   // now check plane by plane, transform back and check that we have the right Gaussian
   for (int x = 0; x < shape[2]; ++x) {
        for (int y = 0; y < shape[3]; ++y) {
            const casacore::IPosition blc(4, 0, 0, x, y);
            const casacore::IPosition trc(4, shape[0] - 1, shape[1] - 1, x, y);
            // here take a copy - this will leave the original array unmodified
            Matrix<typename FTTypeHelper<T>::FT> buffer2 = buffer(blc,trc).nonDegenerate(2).copy();
            wrapper(buffer2, false);
            checkGaussian(buffer2, static_cast<T>(x * y) / 10.f, off_row, off_col, static_cast<T>(8.) + x, static_cast<T>(9.) + y);
        }
   }
   // for completeness, transform back as an array and check again directly
   wrapper.transformAllHyperPlanes(buffer, false);
   for (int x = 0; x < shape[2]; ++x) {
        for (int y = 0; y < shape[3]; ++y) {
            const casacore::IPosition blc(4, 0, 0, x, y);
            const casacore::IPosition trc(4, shape[0] - 1, shape[1] - 1, x, y);
            // take a slice this time (i.e. no copy/use reference semantics)
            Matrix<typename FTTypeHelper<T>::FT> slice = buffer(blc,trc).nonDegenerate(2);
            checkGaussian(slice, static_cast<T>(x * y) / 10.f, off_row, off_col, static_cast<T>(8.) + x, static_cast<T>(9.) + y);
        }
   }
}

/// @brief test with simple sine wave
void FFT2DTest::testSineWave() 
{
   FFT2DWrapper<casacore::Complex> spWrapper;
   testNewWrapperSineWave(spWrapper, 135, 135, 9, 9);
   testNewWrapperSineWave(spWrapper, 135, 45, 9, 3);
   testNewWrapperSineWave(spWrapper, 128, 45, 8, 3);
   testNewWrapperSineWave(spWrapper, 90, 90, 9, 6);

   FFT2DWrapper<casacore::DComplex> dpWrapper;
   testNewWrapperSineWave(dpWrapper, 135, 135, 9, 9);
   testNewWrapperSineWave(dpWrapper, 135, 45, 9, 3);
   testNewWrapperSineWave(dpWrapper, 128, 45, 8, 3);
   testNewWrapperSineWave(dpWrapper, 90, 90, 9, 6);
}

/// @brief test with simple sine wave
/// @details Generates a sine wave, does FFT, checks the results + same with inverse FFT
/// @param[in] wrapper wrapper to work with, sharing it between multiple calls enables to test caching as well
/// @param[in] size_x size of the grid on the first axis
/// @param[in] size_y size of the grid on the second axis
/// @param[in] freq_x frequency of the sine wave on the first axis (in pixels after FFT)
/// @param[in] freq_y frequency of the sine wave on the second axis (in pixels after FFT)
template<typename FT>
void FFT2DTest::testNewWrapperSineWave(FFT2DWrapper<FT> &wrapper, casacore::uInt size_x, casacore::uInt size_y, casacore::uInt freq_x, casacore::uInt freq_y)
{
   const casacore::Int halfSzRow = size_x / 2;
   const casacore::Int halfSzCol = size_y / 2;
   const double factor_x = casacore::C::_2pi*static_cast<double>(freq_x) / size_x;
   const double factor_y = casacore::C::_2pi*static_cast<double>(freq_y) / size_y;
   casacore::Matrix<FT> buffer(size_x, size_y, 0.);
   for (casacore::uInt row = 0; row < buffer.nrow(); ++row) {
        const double valRow = sin((static_cast<casacore::Int>(row) - halfSzRow) * factor_x);
        for (casacore::uInt col = 0; col < buffer.ncolumn(); ++col) {
             const double valCol = cos((static_cast<casacore::Int>(col) - halfSzCol) * factor_y);
             buffer(row,col) = static_cast<typename FT::value_type>(valRow * valCol);
        }
   }
   // test inverse FFT as well
   casacore::Matrix<FT> bufferInv(buffer.copy());
    
   //fft2d(buffer, true);
   wrapper(buffer, true);
   //saveAsCasaImage("tst.img", casacore::imag(buffer));

   // test the result
   CPPUNIT_ASSERT(freq_x < halfSzRow);
   CPPUNIT_ASSERT(freq_y < halfSzCol);
   const typename FT::value_type expectedAbsVal = static_cast<typename FT::value_type>(size_x * size_y) / 4;
   const typename FT::value_type tolerance = getTolerance<typename FT::value_type>() * expectedAbsVal * 4;
   // all values should be pure imaginary and zeros outside of 4 points
   //std::cout<<"max_real: "<<casacore::max(casacore::abs(casacore::real(buffer)))<<std::endl;
   //typename FT::value_type temp_max=0;
   for (casacore::uInt row = 0; row < buffer.nrow(); ++row) {
        for (casacore::uInt col = 0; col < buffer.ncolumn(); ++col) {
             CPPUNIT_ASSERT_DOUBLES_EQUAL(0., casacore::real(buffer(row, col)), tolerance);
             if ((abs(static_cast<int>(row) - halfSzRow) != freq_x) && (abs(static_cast<int>(col) - halfSzCol) != freq_y)) {
                 CPPUNIT_ASSERT_DOUBLES_EQUAL(0., casacore::imag(buffer(row, col)), tolerance);
                 //const typename FT::value_type curVal = casacore::abs(casacore::imag(buffer(row,col)));
                 //if (curVal > temp_max) { temp_max = curVal;}
             }
        }
   }
   //std::cout<<"max_imag(at zero points): "<<temp_max<<std::endl;
   
   CPPUNIT_ASSERT_DOUBLES_EQUAL(expectedAbsVal, casacore::imag(buffer(halfSzRow - freq_x, halfSzCol - freq_y)), tolerance);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(-expectedAbsVal, casacore::imag(buffer(halfSzRow + freq_x, halfSzCol - freq_y)), tolerance);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(expectedAbsVal, casacore::imag(buffer(halfSzRow - freq_x, halfSzCol + freq_y)), tolerance);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(-expectedAbsVal, casacore::imag(buffer(halfSzRow + freq_x, halfSzCol + freq_y)), tolerance);
 

   // test inverse FFT as well with the same input (which was copied before the forward FFT)
   wrapper(bufferInv, false);
   //fft2d(bufferInv, false);
   //saveAsCasaImage("tst.img", casacore::imag(bufferInv));

   // the result should only be differ by scaling with the number of pixels and the sign
   bufferInv *= -static_cast<typename FT::value_type>(size_x * size_y);
   for (casacore::uInt row = 0; row < buffer.nrow(); ++row) {
        for (casacore::uInt col = 0; col < buffer.ncolumn(); ++col) {
             CPPUNIT_ASSERT_DOUBLES_EQUAL(casacore::real(buffer(row,col)), casacore::real(bufferInv(row,col)), tolerance);
             CPPUNIT_ASSERT_DOUBLES_EQUAL(casacore::imag(buffer(row,col)), casacore::imag(bufferInv(row,col)), tolerance);
        }
   }
}

/// @brief test assumptions about FFTW types/their match to casacore types
void FFT2DTest::testTypeAssumptions()
{
   CPPUNIT_ASSERT_EQUAL(sizeof(casacore::Complex), sizeof(FFTWrapperTypeTraits<casacore::Complex>::complex_type));
   CPPUNIT_ASSERT_EQUAL(sizeof(casacore::DComplex), sizeof(FFTWrapperTypeTraits<casacore::DComplex>::complex_type));
   CPPUNIT_ASSERT_EQUAL(2*sizeof(casacore::Complex::value_type), sizeof(FFTWrapperTypeTraits<casacore::Complex>::complex_type));
   CPPUNIT_ASSERT_EQUAL(2*sizeof(casacore::DComplex::value_type), sizeof(FFTWrapperTypeTraits<casacore::DComplex>::complex_type));

   // if the following fails, it fails at compilation
   FFTWrapperTypeTraits<casacore::Complex>::shared_plan_ptr  sptr1;
   FFTWrapperTypeTraits<casacore::DComplex>::shared_plan_ptr  sptr2;
   // just check that they are indeed empty shared pointers
   CPPUNIT_ASSERT(!sptr1);
   CPPUNIT_ASSERT(!sptr2);

   // the following is not really necessary as we never use this equivalence directly, 
   // but do it anyway as an illustration how appropriate types can be deduced
   CPPUNIT_ASSERT_EQUAL(sizeof(void *), sizeof(FFTWrapperTypeTraits<casa::Complex>::plan_type));
   CPPUNIT_ASSERT_EQUAL(sizeof(void *), sizeof(FFTWrapperTypeTraits<casa::DComplex>::plan_type));
}

void FFT2DTest::testSharedPlanPointers()
{
   const int size = 1024;
   // plan should be destroyed when control gets outside the block
   {
      FFT2DWrapper<casacore::Complex> wrapper(false);
      typedef FFTWrapperTypeTraits<casacore::Complex>::complex_type complex_type;
      boost::shared_array<complex_type> buffer(new complex_type[size]);
      FFTWrapperTypeTraits<casacore::Complex>::shared_plan_ptr  sptr = wrapper.makeSharedPlanPtr(fftwf_plan_dft_1d(size,buffer.get(), buffer.get(), FFTW_FORWARD, FFTW_ESTIMATE));
      CPPUNIT_ASSERT(sptr);
   }
   // plan should be destroyed when control gets outside the block
   {
      FFT2DWrapper<casacore::DComplex> wrapper(false);
      typedef FFTWrapperTypeTraits<casacore::DComplex>::complex_type complex_type;
      boost::shared_array<complex_type> buffer(new complex_type[size]);
      FFTWrapperTypeTraits<casacore::DComplex>::shared_plan_ptr  sptr = wrapper.makeSharedPlanPtr(fftw_plan_dft_1d(size,buffer.get(), buffer.get(), FFTW_FORWARD, FFTW_ESTIMATE));
      CPPUNIT_ASSERT(sptr);
   }
}

void FFT2DTest::testExperiments()
{
   const float xOff1 = -10, xOff2 = +23;
   const float yOff1 = 5, yOff2 = -14.5;
   const float width = 4.;
   const casacore::uInt size = 12880;

   casacore::Matrix<casacore::Complex> buffer(size, size, 0.f);
   utility::addGaussian(buffer, 1.f, 0.f, 0.f, width, width);
   utility::addGaussian(buffer, 1.f, xOff1, yOff1, width, width);
   utility::addGaussian(buffer, 1.f, xOff2, yOff2, width, width);
   //saveAsCasaImage("tst.img", casacore::real(buffer));
   FFT2DWrapper<casacore::Complex> wrapper(true);
   casacore::Timer timer;
   timer.mark();
   wrapper(buffer, true);
   //fft2d(buffer, true);
   std::cout<<"FFT took: "<<timer.real()<<" seconds"<<std::endl;
   timer.mark();
   wrapper.fftShift(buffer, true);
   std::cout<<"FFT shift took: "<<timer.real()<<" seconds"<<std::endl;
}

/// @brief test of conjugation property for real input
/// @details It is one of the best ways to verify that harmonics are placed where we expect them.
/// Three Gaussian sources are simulated, one at the origin and two with some offsets.
/// @param[in] size size of the grid to work with (same for both axes)
/// @param[in] width width of the Gaussian in pixels (same for both axes and all sources)
template<typename T>
void FFT2DTest::testConjugation(casacore::uInt size, T width)
{
   const T xOff1 = -10, xOff2 = +23;
   const T yOff1 = 5, yOff2 = -14.5;
   const T zero = 0.;
   const T one = 1.;

   casacore::Matrix<typename FTTypeHelper<T>::FT> buffer(size, size, zero);
   utility::addGaussian(buffer, one, zero, zero, width, width);
   utility::addGaussian(buffer, one, xOff1, yOff1, width, width);
   utility::addGaussian(buffer, one, xOff2, yOff2, width, width);
   //saveAsCasaImage("tst.img", casacore::real(buffer));
   fft2d(buffer, true);
   //saveAsCasaImage("tst.img", casacore::real(buffer));
   // for odd number of points we have N/2 positive and N/2 negative harmonics,
   // for an even number there is one more negative
   const casacore::Int centre = size / 2;
   ASKAPDEBUGASSERT(centre > 1);
   const casacore::Int halfsz = centre - (size % 2 == 0 ? 1: 0);
   for (casacore::Int x=-halfsz; x<=+halfsz; ++x) {
        for (casacore::Int y=-halfsz; y<=+halfsz; ++y) {
             ASKAPASSERT(centre+x >= 0);
             ASKAPASSERT(centre-x >= 0);
             ASKAPASSERT(centre+x < buffer.nrow());
             ASKAPASSERT(centre-x < buffer.nrow());
             ASKAPASSERT(centre+y >= 0);
             ASKAPASSERT(centre-y >= 0);
             ASKAPASSERT(centre+y < buffer.ncolumn());
             ASKAPASSERT(centre-y < buffer.ncolumn());
             const typename FTTypeHelper<T>::FT val1 = buffer(centre+x, centre+y);
             const typename FTTypeHelper<T>::FT val2 = std::conj(buffer(centre-x, centre-y));
             // scale tolerance with the grid size, kind of worst case scenario of dynamic range limitation
             CPPUNIT_ASSERT_DOUBLES_EQUAL(casacore::real(val1), casacore::real(val2), getTolerance<T>() * size);
             CPPUNIT_ASSERT_DOUBLES_EQUAL(casacore::imag(val1), casacore::imag(val2), getTolerance<T>() * size);
        }
   }
}

void FFT2DTest::testNewWrapperSinglePrecision()
{
   const casacore::Int sz = 128;
   const casacore::Float width = 4.;
   testNewWrapper(sz, sz, width, width);
}

void FFT2DTest::testNewWrapperDoublePrecision()
{
   const casacore::Int sz = 128;
   const casacore::Double width = 4.;
   testNewWrapper(sz, sz, width, width);
}



void FFT2DTest::testNewWrapperOddSizeSinglePrecision()
{
   const casacore::Int szx = 135;
   const casacore::Int szy = 63;
   const casacore::Float widthx = 4.;
   const casacore::Float widthy = 3.;
   testNewWrapper(szx, szy, widthx, widthy);
} 
 
void FFT2DTest::testShiftSinglePrecision() 
{
   const casacore::Int sz = 128;
   const casacore::Float width = 4.;
   testShift(sz, sz, width, width);
}

void FFT2DTest::testShiftDoublePrecision() 
{
   const casacore::Int sz = 128;
   const casacore::Double width = 4.;
   testShift(sz, sz, width, width);
}

void FFT2DTest::testShiftSinglePrecision2() 
{
   // same as testShiftSinglePrecision, but with a rectangular image and elliptical Gaussian
   const casacore::Int sz = 128;
   const casacore::Float width = 4.;
   testShift(sz, sz*2, width, width*3);
}

void FFT2DTest::testShiftDoublePrecision2() 
{
   const casacore::Int sz = 128;
   const casacore::Double width = 4.;
   testShift(sz, sz*2, width, width*3);
}


/// @brief single precision test of conjugation property
/// @details Essentially it verifies that harmonic placement is as expected
void FFT2DTest::testConjSinglePrecision()
{
   testConjugation(128., 4.f);
}

/// @brief double precision test of conjugation property
/// @details Essentially it verifies that harmonic placement is as expected
void FFT2DTest::testConjDoublePrecision()
{
   testConjugation(128., static_cast<casacore::Double>(4.));
}

/// @brief single precision test of conjugation property for an odd image size
/// @details Essentially it verifies that harmonic placement is as expected
void FFT2DTest::testConjOddSizeSinglePrecision()
{
   testConjugation(189., 4.f);
}

/// @brief double precision test of conjugation property for an odd image size
/// @details Essentially it verifies that harmonic placement is as expected
void FFT2DTest::testConjOddSizeDoublePrecision()
{
   testConjugation(135., static_cast<casacore::Double>(4.));
}


} // namespace scimath

} // namespace askap

#endif // #ifndef SCIMATH_2DFFT_TEST

