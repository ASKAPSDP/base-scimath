/// @file
///
/// Collection of utility methods specific to FFT. We could've made them static members of the wrapper class.
/// but they don't conceptually belong there (e.g. do not depend on the template parameter).
///
///
/// @copyright (c) 2023 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>

#ifndef ASKAP_SCIMATH_FFT_FFT_UTILS_H
#define ASKAP_SCIMATH_FFT_FFT_UTILS_H

namespace askap {

namespace scimath {

/// @brief Find good FFT size not smaller than the given number
/// @details Performance of FFTW depends strongly on the size of the grid (exact composition in
/// the prime number factors, i.e. +/- few pixels can make a big difference). This method finds
/// an even number, not smaller than the given limit, with only factors of 2,3,5,7. According to
/// the FFTW docs, this is the best grid size performance wise.
/// @param[in] lowerLimit the smallest number this method can return
/// @return int smallest even number with only factors of 2,3,5,7 >= limit parameter
/// @note both parameter and return value are sign integers as this is typically passed to casacore's 
/// IPosition or array interface, but we don't expect to deal with non-positive numbers here (and there is
/// actually an assert to check this in the implementation)
int goodFFTSize(int lowerLimit);


} // namespace scimath

} // namespace askap

#endif // #ifndef ASKAP_SCIMATH_FFT_FFT_UTILS_H
