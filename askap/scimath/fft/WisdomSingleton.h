/// @file
///
/// This singleton class controls management of FFTW wisdom files (which is a global thing due to
/// the FFTW interface, and hence the use of the singleton pattern). The idea is similar ThreadInitSingleton class, so
/// it was rather tempting just to add the appropriate functionality / methods there. But I (MV) think it is cleaner to 
/// have wisdom files managed by a separate class and avoid fat interfaces / blurring responsibilities between different
/// classes. We decided against clean up of the wisdom information. It is small, cumulative and doesn't need an explicit
/// free up according to the documentation. So, no guard classes similar to those used for threading initialisation are
/// required. However, we reuse the integer tag from the guard classes here to store information per precision option,
/// in a similar fashion to ThreadInitSingleton class. Another difference is the lack of thread synchronisation. 
/// We expect to use this class alongside the plan creation which is already protected by the mutex. So no additional
/// synchronisation is necessary, but care must be taken if this singleton is used directly (rather than through the 
/// FFT wrapper). It looks like FFTW wisdom file is specific to the number of threads used in the parallel case. The main
/// role of this class is to manage this complexity and the two precision options. It is expected that wisdom files 
/// have the name "fftw-[double|float]-Xthreads.wisdom" where X is the number of OpenMP threads without leading zeros.
/// The wisdom files are expected to be located in the directory pointed to by FFTW_WISDOM environment variable. If the
/// variable is not defined, loading wisdom files will be inhibited and FFTs will resort to the usual ESTIMATE method.
/// Otherwise, the loading of wisdom file will be attempted at the beginning and every time the number of available threads
/// changes, separately for each available precision option. If the corresponding file is missing, FFTs will resport to the
/// ESTIMATE method and no further attempts to read this file will be made. If reading the file fails, an exception will be 
/// thrown.
///
/// As for ThreadInitSingleton, the check of the initialisation state is done via the map key. Note, the size of such map 
/// is expected to be very small (only 2 elements in our current use; one element per supported FFTW type), so
/// hopefully no performance impact. If necessary, it can be reimplemented through other means (e.g. a static array).
/// The current way is just rather general and probably results in a more readable code too. 
///
/// This singleton class is used behind the scene by the FFTW wrapper, so the end user should rarely worry about it.
/// For the reference, the typical usage pattern involve calling the static getInstance() method to get the singleton itself and
/// then a templated loadWisdomFileIfNecessary<FT>(numThreads) method for the type of interest (note, typing is done by 
/// the Fourier type, i.e. a complex of some sort) just before plan creation. The parameter passed to the method is the number of
/// available threads.
///
/// \code{.cpp}
///    WisdomSingleton::getInstance().loadWisdomFileIfNecessary<casacore::DComplex>(numThreads);
/// \endcode
///
/// @note The fact that we have to create two separate singleton classes most likely suggests that we a better design could be just
/// one singleton corresponding to the whole FFTW library managing all its global data and precision options. It could have methods
/// returning handler objects for threading or wisdom management as appropriate. We may change the design if there is a need to 
/// integrate more operations like this.
///
///
/// @copyright (c) 2023 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>

#ifndef ASKAP_SCIMATH_FFT_WISDOM_SINGLETON_H
#define ASKAP_SCIMATH_FFT_WISDOM_SINGLETON_H

// boost includes
#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/noncopyable.hpp>

// std includes
#include <map>
#include <string>

namespace askap {

namespace scimath {

// for unit tests
class WisdomTest;

/// @brief This is a singleton class which controls management of wisdom files in FFTW
/// @details
/// This singleton class controls management of FFTW wisdom files (which is a global thing due to
/// the FFTW interface, and hence the use of the singleton pattern). The idea is similar ThreadInitSingleton class, so
/// it was rather tempting just to add the appropriate functionality / methods there. But I (MV) think it is cleaner to 
/// have wisdom files managed by a separate class and avoid fat interfaces / blurring responsibilities between different
/// classes. We decided against clean up of the wisdom information. It is small, cumulative and doesn't need an explicit
/// free up according to the documentation. So, no guard classes similar to those used for threading initialisation are
/// required. However, we reuse the integer tag from the guard classes here to store information per precision option,
/// in a similar fashion to ThreadInitSingleton class. Another difference is the lack of thread synchronisation. 
/// We expect to use this class alongside the plan creation which is already protected by the mutex. So no additional
/// synchronisation is necessary, but care must be taken if this singleton is used directly (rather than through the 
/// FFT wrapper). It looks like FFTW wisdom file is specific to the number of threads used in the parallel case. The main
/// role of this class is to manage this complexity and the two precision options. It is expected that wisdom files 
/// have the name "fftw-[double|float]-Xthreads.wisdom" where X is the number of OpenMP threads without leading zeros.
/// The wisdom files are expected to be located in the directory pointed to by FFTW_WISDOM environment variable. If the
/// variable is not defined, loading wisdom files will be inhibited and FFTs will resort to the usual ESTIMATE method.
/// Otherwise, the loading of wisdom file will be attempted at the beginning and every time the number of available threads
/// changes, separately for each available precision option. If the corresponding file is missing, FFTs will resport to the
/// ESTIMATE method and no further attempts to read this file will be made. If reading the file fails, an exception will be 
/// thrown.
///
/// As for ThreadInitSingleton, the check of the initialisation state is done via the map key. Note, the size of such map 
/// is expected to be very small (only 2 elements in our current use; one element per supported FFTW type), so
/// hopefully no performance impact. If necessary, it can be reimplemented through other means (e.g. a static array).
/// The current way is just rather general and probably results in a more readable code too. 
///
/// This singleton class is used behind the scene by the FFTW wrapper, so the end user should rarely worry about it.
/// For the reference, the typical usage pattern involve calling the static getInstance() method to get the singleton itself and
/// then a templated loadWisdomFileIfNecessary<FT>(numThreads) method for the type of interest (note, typing is done by 
/// the Fourier type, i.e. a complex of some sort) just before plan creation. The parameter passed to the method is the number of
/// available threads.
///
/// \code{.cpp}
///    WisdomSingleton::getInstance().loadWisdomFileIfNecessary<casacore::DComplex>(numThreads);
/// \endcode
///
/// @note The fact that we have to create two separate singleton classes most likely suggests that we a better design could be just
/// one singleton corresponding to the whole FFTW library managing all its global data and precision options. It could have methods
/// returning handler objects for threading or wisdom management as appropriate. We may change the design if there is a need to 
/// integrate more operations like this.
///
/// @ingroup fft
class WisdomSingleton : public boost::noncopyable {
   /// @brief private constructor
   /// @details the object is not to be created except via the getInstance() method. The constructor
   /// reads FFTW_WISDOM environment variable and caches the directory path for future use.
   WisdomSingleton() : itsWisdomDirectory(obtainWisdomDirectory()) {}
public:
   /// @brief get instance method of the singleton
   /// @details As per singleton design pattern with lazy initialisation, an instance is created on the first use
   /// @return a non-const reference to the singleton object
   static WisdomSingleton& getInstance();

   /// @brief templated method to load wisdom file for the given number of threads
   /// @details The template parameter is the type of Fourier space point to define precision option.
   /// @param[in] numThreads the number of available OpenMP threads (to load the appropriate file)
   /// @note We could've used the appropriate real space type, but it is better this way for
   /// consistency with the rest of the wrapper code. 
   template<typename FT>
   void loadWisdomFileIfNecessary(int numThreads);

   /// @brief cached directory path where the wisdom files are expected to be
   /// @return path to the directory with wisdom files or an empty string if not setup
   inline const std::string& wisdomDirectory() const { return itsWisdomDirectory; }

   /// @brief helper method to check that loading of wisdom files is configured
   /// @details Loading of wisdom files only happens if FFTW_WISDOM variable is defined. This
   /// is a helper method to check that this is the case. 
   /// @return true if the class will attempt loading wisdom files in loadWisdomFileIfNecessary and if
   /// wisdomFileName method can be used. 
   /// @note true returned by this method doesn't mean that some wisdom file has already been loaded or even exists
   inline bool wisdomConfigured() const { return itsWisdomDirectory.size() > 0u; }

   /// @brief obtain wisdom file name corresponding to the given number of threads
   /// @details The wisdom files are expected to have the name "fftw-[double|float]-Xthreads.wisdom" where X 
   /// is the number of OpenMP threads without leading zeros and be located in the directory pointed by FFTW_WISDOM
   /// environment variable. This method returns the full name to the file translating the precision option and
   /// the given number of threads. It throws an exception if the support of wisdom files has not been configured.
   /// Use wisdomConfigured method to check if this is the case before calling this one.
   /// @param[in] numThreads the number of available OpenMP threads (to be embedded in the file name)
   /// @return full path to the wisdom file name (it may or may not exist)
   /// @note This method is templated to be able to embed floating point precision in the file name 
   /// (e.g. double or float as defined in the traits class). The template parameter is the type of the point in the
   /// Fourier space (i.e. some kind of complex), as in the other parts of this wrapper.
   template<typename FT>
   std::string wisdomFileName(int numThreads) const;

   /// @brief low level method to export a wisdom file
   /// @details It is handy to keep all wisdom-related operations in this class. To be able to generate wisdom files we
   /// need to write them eventually after the appropriate planning is done. We expect to use this method from the specialised
   /// routine generating wisdom files. In normal operations (when we just read wisdom files), it is not called. As the case of
   /// all methods of this class, there is no concurrency protention of any kind, so the user should worry about thread synchronisation
   /// if this method is used in parallel environment.
   /// @note This method just calles the appropriate FFTW routine for the given data type. It doesn't require FFTW_WISDOM
   /// variable to be defined, but we expect to use it with a name generated by wisdomFileName which does require FFTW_WISDOM to be
   /// defined. Any error returned by the FFTW routine is a genuine one and will generate an exception.
   /// @param[in] fileName name of the wisdom file to write
   template<typename FT>
   static void writeWisdomFile(const std::string &fileName);

private:
   /// @brief actual low level method to load wisdom file
   /// @details It just calls the appropriate FFTW method based on the template argument and checks that
   /// it doesn't error. We verify that the wisdom file exists before calling this method, so any error is a genuine error
   /// and will generate an exception. 
   /// @note Unlike the method for wisdom file writing, this method is not expected to be called directly (and therefore is made
   /// private). Use loadWisdomFileIfNecessary instead.
   /// @param[in] fileName name of the wisdom file to load
   template<typename FT>
   static void loadWisdomFile(const std::string &fileName);

   /// @brief put string message to the log 
   /// @details I (MV) found that our logging macros are not very suitable for templated code due to the need to define
   /// a global logger instance (templates can be compiled multiple times in different translation units). One can either
   /// have a method returning a reference to the logger instantiated in the cc file or a wrapper method like this to 
   /// do the logging outside of the template environment. 
   /// @param[in] msg string message to put into the log (with INFO severity)
   static void logMessage(const std::string &msg);


   /// @brief read FFTW_WISDOM environment variable
   /// @details This method returns the value of FFTW_WISDOM environment variable.
   /// If it is defined, checks are done that the content is a valid path to a directory.
   /// @note this method is expected to be called from the private constructor only (although there is no
   /// harm doing it in other situations if we'd like). Later on, the result can be accessed via
   /// wisdomDirectory method which returns the cached value.
   /// @return path to the wisdom directory as indicated by FFTW_WISDOM
   static std::string obtainWisdomDirectory();

   /// @brief helper method to check that the given file exists
   /// @details Although we use it only once, it helps with the clarity of the code to have this functionality
   /// gathered in a separate method.
   /// @param[in] fileName file name to check
   /// @return true if the file exists, false otherwise
   static bool exists(const std::string &fileName);

   /// @brief key singleton feature - static pointer to the instance
   static boost::shared_ptr<WisdomSingleton>  theirInstance;

   // note, no thread synchronisation for the methods of this class is necessary because we don't expect to
   // use it in concurrent environment (that part of the code is already mutex-protected)

   /// @brief map with the number of threads for already loaded wisdom files
   /// @details The key of the map is an integer tag provided by reused thread initialiser guard type
   /// (defined in the traits class) ensuring that we read wisdom file only once for each precision option.
   /// The value is just the number of available OpenMP threads for which the previous import of wisdom
   /// files was performed. It looks like FFTW rejects wisdom data if the current number of available threads
   /// is less than what was at the time of wisdom file creation, so we have to keep track.
   /// The absence of the element with the appropriate tag means that no wisdom file has been read yet.
   std::map<int, int> itsNumThreadsForWisdom;

   /// @brief path to directory with wisdom files
   /// @details This is essentially the content of FFTW_WISDOM environment variable which is read in the constructor.
   /// Empty string means that loading wisdom has not been setup (i.e. FFTW_WISDOM is empty)
   const std::string itsWisdomDirectory;

   friend WisdomTest;
};

} // namespace scimath

} // namespace askap

// implementation of the templated methods, non-templated methods and static members live in the cc file ensuring they are common
// for all translation units
#include <askap/scimath/fft/WisdomSingleton.tcc>

#endif // #ifndef ASKAP_SCIMATH_FFT_WISDOM_SINGLETON_H
