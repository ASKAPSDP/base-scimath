/// @file
///
/// This is a helper template enabling type translation from casacore types to types used by FFTW. 
/// It makes the code more generic and less hacky when we interface to low-level C library like FFTW.
///
/// @copyright (c) 2022 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>

#ifndef ASKAP_SCIMATH_FFT_FFT_WRAPPER_TYPE_TRAITS_H
#define ASKAP_SCIMATH_FFT_FFT_WRAPPER_TYPE_TRAITS_H

// casacore includes
#include <casacore/casa/BasicSL/Constants.h>
#include <casacore/casa/BasicSL/Complex.h>
// FFTW
#include <fftw3.h>

// boost includes
#include <boost/shared_ptr.hpp>
#include <boost/type_traits.hpp>
#include <boost/thread/mutex.hpp>

// std includes
#include <string>

namespace askap {

namespace scimath {

/// @brief base guard class for threading initialisation/destruction
/// @details Actual resource allocation and deallication take place in 
/// derived classes. We need a common base to allow both single and double
/// precision versions to be used concurrently
struct ThreadInitGuardBase {
   /// @brief empty virtual destructor
   /// @details This is added to ensure the destructor of the actual (derived) type is called as
   /// we expect to manipulate the types polymorphically (i.e. delete the actual object by the
   /// shared pointer to the base class). Otherwise it would result in an undefined behaviour
   /// (MV: although seems to do the right thing with our compilers, probably because these types don't
   /// have ordinary virtual functions)
   virtual ~ThreadInitGuardBase() {}
};

// by default, no types are defined. This will break the compilation
template<typename FT>
struct FFTWrapperTypeTraits {
};

// specialisation for DComplex
template<>
struct FFTWrapperTypeTraits<casacore::DComplex> {
   /// @brief matching fftw complex type
   typedef fftw_complex complex_type;

   /// @brief matching fftw plan type
   typedef fftw_plan plan_type;
 
   /// @brief shared pointer to plan
   /// @details Using shared pointers we can use the compiler to ensure the plan is deallocated
   /// @note This relies on implementation detail that plan_type is indeed a raw pointer. Although
   /// the definition itself will work for any type, it will likely produce a failure when such
   /// shared pointer is used (but it can be worked around with further template magic)
   typedef boost::shared_ptr<boost::remove_pointer<fftw_plan>::type> shared_plan_ptr;

   /// @brief precision given as a character string
   /// @details This is used to compose wisdom file names in a templated method. Note, we could've
   /// made it a constexpr in a more modern C++ with string_view, but for older C++ we're using it is
   /// easier to make it a static method (which will probably be optimised by the compiler anyway)
   /// @return string describing precision 
   static std::string precisionString() { return "double"; }

   /// @brief FFTW plan deleter
   /// @details It would be needed if shared pointer types are used.
   /// It prevents the shared pointer to delete a plan through normal C++ delete and does it with
   /// the special destroy_plan call
   struct PlanDeleter {
       /// @brief construct custom deleter
       /// @param[in] mtx reference to mutex object to use for locking
       explicit PlanDeleter(boost::mutex &mtx) : itsMutex(mtx) {}
       /// @brief delete the plan
       void operator()(plan_type plan) const;
     private:
       /// @brief reference to the mutex to use for when plan is destroyed
       boost::mutex& itsMutex;
   };

   /// @brief FFTW threading initialisation/destruction
   /// @details This class calls appropriate init_threads method of FFTW in constructor and
   /// cleanup_threads in destructor
   struct ThreadInitGuard final : public ThreadInitGuardBase {
      /// @brief unique id for this specialisation
      /// @details to ensure implementation on the user side is generic and doesn't require changes if
      /// we add support of a new type, it is handy to have tag each specialisation with a unique integer constant
      static constexpr int tag = 1;

      /// @brief constructor - allocates resource
      ThreadInitGuard();

      /// @brief destructor- deallocates resource
      virtual ~ThreadInitGuard();
   };
};

// specialisation for Complex
template<>
struct FFTWrapperTypeTraits<casacore::Complex> {
   /// @brief matching fftw complex type
   typedef fftwf_complex complex_type;

   /// @brief matching fftw plan type
   typedef fftwf_plan plan_type;

   /// @brief shared pointer to plan
   /// @details Using shared pointers we can use the compiler to ensure the plan is deallocated
   /// @note This relies on implementation detail that plan_type is indeed a raw pointer. Although
   /// the definition itself will work for any type, it will likely produce the failure when such
   /// shared pointer is used (but it can be worked around with further template magic)
   typedef boost::shared_ptr<boost::remove_pointer<fftwf_plan>::type> shared_plan_ptr;

   /// @brief precision given as a character string
   /// @details This is used to compose wisdom file names in a templated method. Note, we could've
   /// made it a constexpr in a more modern C++ with string_view, but for older C++ we're using it is
   /// easier to make it a static method (which will probably be optimised by the compiler anyway)
   /// @return string describing precision 
   static std::string precisionString() { return "float"; }

   /// @brief FFTW plan deleter
   /// @details It would be needed if shared pointer types are used.
   /// It prevents the shared pointer to delete a plan through normal C++ delete and does it with
   /// the special destroy_plan call
   struct PlanDeleter {
       /// @brief construct custom deleter
       /// @param[in] mtx reference to mutex object to use for locking
       explicit PlanDeleter(boost::mutex &mtx) : itsMutex(mtx) {}
       /// @brief destroys the plan
       void operator()(plan_type plan) const;
     private:
       /// @brief reference to the mutex to use for when plan is destroyed
       boost::mutex& itsMutex;
   };

   /// @brief FFTW threading initialisation/destruction
   /// @details This class calls appropriate init_threads method of FFTW in constructor and
   /// cleanup_threads in destructor
   struct ThreadInitGuard final : public ThreadInitGuardBase {
      /// @brief unique id for this specialisation
      /// @details to ensure implementation on the user side is generic and doesn't require changes if
      /// we add support of a new type, it is handy to have tag each specialisation with a unique integer constant
      static constexpr int tag = 2;

      /// @brief constructor - allocates resource
      ThreadInitGuard();

      /// @brief destructor- deallocates resource
      virtual ~ThreadInitGuard();
   };
};

} // namespace scimath

} // namespace askap

#include <askap/scimath/fft/FFTWrapperTypeTraits.tcc>

#endif // #ifndef ASKAP_SCIMATH_FFT_FFT_WRAPPER_TYPE_TRAITS_H
