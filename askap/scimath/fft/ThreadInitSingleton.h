/// @file
///
/// This singleton class controls initialisation of threading in FFTW (which is a global thing due to
/// the FFTW interface, and hence the use of the singleton pattern). It is very tempting to have a templated
/// implementation effectively to have a separate "singleton" for each precision option available (although this aspect
/// is poorly documented, it looks like we have to initialise for each type before using the appropriate methods) to
/// ensure we only run the appropriate init methods if necessary. However, this is likely to create a problem because
/// our code is distributed across many repos (and, therefore, shared libraries) and templates may be compiled separately
/// in different translation units which could lead to an undefined behaviour. So, the solution is more or less a
/// classical singleton, common for all data types. This singleton manages thread safety and guarantees that there is only 
/// one instance of the object as usual. However, instead of the templated class, there is a templated initialisation method
/// of the singleton which instantiates the appropriate object behind the scene and this object manages the lifecycle of the
/// FFTW threading data through the usual construction-destruction idiom. Individual guard objects (one for each supported
/// FFTW type) are all inherited from a common base guard object to store them easily in the common container 
/// (a map in our case). The check of the initialisation state is done via the map key. Note, the size of such map 
/// is expected to be very small (only 2 elements in our current use; one element per supported FFTW type), so
/// hopefully no performance impact. If necessary, it can be reimplemented through other means (e.g. a static array).
/// The current way is just rather general and probably more readable code too. One could also use polymorphism for the
/// same purpose, but it is unlikely to be better in any way.
///
/// This singleton class is used behind the scene by the FFTW wrapper, so the end user should rarely worry about it.
/// For the reference, the typical usage pattern involve calling the static getInstance() method to get the singleton itself and
/// then a templated init<FT>() method for the type of interest (note, typing is done by the Fourier type, i.e. a complex of some sort)
/// before the first use of FFTW methods with multithreading:
///
/// \code{.cpp}
///    ThreadInitSingleton::getInstance().init<casacore::DComplex>();
/// \endcode
///
/// @copyright (c) 2022 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>

#ifndef ASKAP_SCIMATH_FFT_THREAD_INIT_SINGLETON_H
#define ASKAP_SCIMATH_FFT_THREAD_INIT_SINGLETON_H

// boost includes
#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/noncopyable.hpp>

// std includes
#include <map>

namespace askap {

namespace scimath {

// forward declaration, actually an empty base class (actual object type will always be a ThreadInitGuard template)
class ThreadInitGuardBase;

// for unit tests
class ThreadInitTest;

/// @brief This is a singleton class which controls initialisation of threading in FFTW 
/// @details Threading initialisation is a global thing due to the FFTW interface, and hence the use of 
/// the singleton pattern). It is very tempting to have a templated
/// implementation effectively to have a separate "singleton" for each precision option available (although this aspect
/// is poorly documented, it looks like we have to initialise for each type before using the appropriate methods) to
/// ensure we only run the appropriate init methods if necessary. However, this is likely to create a problem because
/// our code is distributed across many repos (and, therefore, shared libraries) and templates may be compiled separately
/// in different translation units which could lead to an undefined behaviour. So, the solution is more or less a
/// classical singleton, common for all data types. This singleton manages thread safety and guarantees that there is only 
/// one instance of the object as usual. However, instead of the templated class, there is a templated initialisation method
/// of the singleton which instantiates the appropriate object behind the scene and this object manages the lifecycle of the
/// FFTW threading data through the usual construction-destruction idiom. Individual guard objects (one for each supported
/// FFTW type) are all inherited from a common base guard object to store them easily in the common container 
/// (a map in our case). The check of the initialisation state is done via the map key. Note, the size of such map 
/// is expected to be very small (only 2 elements in our current use; one element per supported FFTW type), so
/// hopefully no performance impact. If necessary, it can be reimplemented through other means (e.g. a static array).
/// The current way is just rather general and probably more readable code too. One could also use polymorphism for the
/// same purpose, but it is unlikely to be better in any way.
///
/// This singleton class is used behind the scene by the FFTW wrapper, so the end user should rarely worry about it.
/// For the reference, the typical usage pattern involve calling the static getInstance() method to get the singleton itself and
/// then a templated init<FT>() method for the type of interest (note, typing is done by the Fourier type, i.e. a complex of some sort)
/// before the first use of FFTW methods with multithreading:
///
/// \code{.cpp}
///    ThreadInitSingleton::getInstance().init<casacore::DComplex>();
/// \endcode
///
/// @ingroup fft
class ThreadInitSingleton : public boost::noncopyable {
   /// @brief private constructor
   /// @details the object is not to be created except via the getInstance() method
   ThreadInitSingleton() {}
public:
   /// @brief get instance method of the singleton
   /// @details As per singleton design pattern with lazy initialisation, an instance is created on the first use
   /// @return a non-const reference to the singleton object
   static ThreadInitSingleton& getInstance();

   /// @brief templated init method
   /// @details The template parameter is the type of Fourier space point.
   /// @note We could've used the appropriate real space type, but it is better this way for
   /// consistency with the rest of the wrapper code.
   template<typename FT>
   void init();

private:
   /// @brief key singleton feature - static pointer to the instance
   static boost::shared_ptr<ThreadInitSingleton>  theirInstance;

   /// @brief mutex to protect data members
   /// @details Use one common mutex for simplicity, although, in principle, separate mutex objects
   /// could be used to protect singleton creation and the actual FFTW threading initialisation
   /// (in particular, that second mutex could be per type of FFT; but performance implications of
   /// having just one mutex should be small for our code as we don't mix FFTs with different precision
   /// at the same stage of processing and there are usually other barriers in between)
   static boost::mutex theirMutex;

   /// @brief map with initialiser objects 
   /// @details The key of the map is an integer tag provided by the actual initialiser guard type
   /// ensuring we have only one initialiser per supported type. The value of the map is the actual
   /// guard object whose destructor releases the resources. These are stored by shared pointer to the
   /// common class. The destruction occurs when the singleton goes out of scope, i.e. at the end of
   /// the application execution. 
   /// @note We can add explicit clean up methods, if needed. But it is a can of worms because one has to 
   /// ensure that it is done in the appropriate time of the program execution and no call to FFT happens while
   /// the library is in an uninitialised state.
   std::map<int, boost::shared_ptr<ThreadInitGuardBase> > itsInitGuards;

   friend ThreadInitTest;
};

} // namespace scimath

} // namespace askap

// implementation of the templated method, non-templated methods and static members live in the cc file ensuring they are common
// for all translation units
#include <askap/scimath/fft/ThreadInitSingleton.tcc>

#endif // #ifndef ASKAP_SCIMATH_FFT_THREAD_INIT_SINGLETON_H 
