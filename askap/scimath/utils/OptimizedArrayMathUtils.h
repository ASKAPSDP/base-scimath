/// @file
/// 
/// @brief helper functions to do math of casa arrays in an optimised way
/// @details This file provides some optimised operations for casa arrays.        
///
///
/// @copyright (c) 2007 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>

#ifndef ASKAP_UTILITY_OPTIMIZED_ARRAY_MATH_UTILS_H
#define ASKAP_UTILITY_OPTIMIZED_ARRAY_MATH_UTILS_H

// casacore includes
#include <casacore/casa/Arrays/Matrix.h>
#include <casacore/casa/Arrays/Array.h>

namespace askap {

namespace utility {

/// @brief multiply matrix in situ by conjugated other matrix element by element
/// @details This is essentially what the A*=conj(B) does, but avoids
/// creating a temporary object for conj(B) reducing peak memory requirements.
/// In addition, some optimisations are done assuming contiguous memory arrays,
/// which is our most common use case in yandasoft.
/// @note We could've implemented it for the array in general, but matrix-specific
/// implementation seems to match our use case. Also note, that this is not a matrix multiplication
/// as per linear algebra, but rather element-by-element multiplication
/// @param[inout] in a matrix to change (in situ), i.e. A in A*=conj(B)
/// @param[in] other a matrix with elements to conjugate and multiply by (i.e B in A*=conj(B))
/// @ingroup utils
template<typename FT>
void multiplyByConjugate(casacore::Matrix<FT> &in, const casacore::Matrix<FT> &other);

/// @brief in place element by element array conjugation
/// @details This method conjugates every element of a complex array in an efficient manner.
/// @param[inout] in array to work with
/// @note This method relies on contiguous storage (and checks this) and also on the fact that complex number
/// is stored as two floats or doubles, with first being the real part and the second being the imaginary one. 
/// It assumes std::complex or equivalent (e.g. casacore types) and won't compile for real arrays.
template<typename FT>
void conjugateComplexArray(casacore::Array<FT> &in);

/// @brief essentially an in place element by element multiplication of several arrays
/// @details This helper method implements the following operation in an optimised way
/// result = conj(A) * B * C / normFactor
/// where result and C are both casacore Arrays with identical shapes (degenerate dimensions are allowed) and
/// B and C are instances of casacore Matrix with the same shape as the arrays for the first two dimensions.
/// We could've had all parameters as arrays or matrices but doing it this way minimises the need for type conversion or
/// handling of degenerate dimensions by matching what we do in our current code.  
/// @param[out] result resulting array to write, must be initialised to the correct shape, old values are not used
/// @param[in] A matrix A in the above formula
/// @param[in] B matrix B in the above formula
/// @param[in] C array C in the above formula
/// @param[in] normFactor a scalar to divide by, see the above formula, the type should match the complex type used
/// @note All arrays and matrices should have contiguous storage, otherwise an exception is thrown
template<typename FT>
void calculateNormalisedProduct(casacore::Array<FT> &result, const casacore::Matrix<FT> &A, const casacore::Matrix<FT> &B, const casacore::Array<FT> &C, 
                                typename FT::value_type normFactor);

/// @brief helper method to calculate the real part of the mean of a complex Array
/// @details This method is equivalent to sum(real(in)) / in.nelements(). Only contiguous arrays are supported.
/// Only arrays of std::complex like types are supported, it is a requirement that the complex type is equivalent to two 
/// adjacent floating point values with the first being the real part (this is the case for both std and casacore types).
/// @param[in] in array to work with
/// @return mean of the real part of the array
template<typename FT>
typename FT::value_type realPartMean(const casacore::Array<FT> &in);

/// @brief add a Gaussian to the buffer provided
/// @details This method simulates 2D Gaussian with parameters provided and adds it to the 
/// buffer (which can be both complex and real)
/// @param[in] buffer the buffer to work with
/// @param[in] peak amplitude of the peak
/// @param[in] row_off offset in the first coordinate in pixels
/// @param[in] col_off offset in the second coordinate in pixels
/// @param[in] row_width width along the first coordinate in pixels
/// @param[in] col_width width along the second coordinate in pixels
/// @note this method is heavily used/tested in FFT2DTest
template <typename FT, typename T = typename FT::value_type>
void addGaussian(casacore::Matrix<FT> &buffer, T peak, T row_off, T col_off, T row_width, T col_width);

/// @brief calculate square for an abstract type
/// @param[in] in input to square
/// @return result of a product by itself
/// @note we coul've used casacore's version instead
template<typename T>
T square(T in) { return in * in;}

} // namespace utility

} // namespace askap

#include <askap/scimath/utils/OptimizedArrayMathUtils.tcc>

#endif // #ifndef ASKAP_UTILITY_OPTIMIZED_ARRAY_MATH_UTILS_H

