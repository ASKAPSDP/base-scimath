/// @file
///
/// Utils for the LSQR solver with smoothing constraints.
///
/// @copyright (c) 2007 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Vitaliy Ogarko <vogarko@gmail.com>
///
#include <askap/scimath/fitting/LinearSolverLsqrUtils.h>
#include <askap/scimath/fitting/LinearSolverUtils.h>
#include <askap/scimath/fitting/CalParamNameHelper.h>
#include <askap/scimath/lsqr_solver/ParallelTools.h>

#include <askap/askap/AskapError.h>
#include <askap/profile/AskapProfiler.h>

#include <askap/askap/AskapLogging.h>
ASKAP_LOGGER(logger, ".lsqrutils");

#include <iostream>
#include <string>
#include <map>

namespace askap { namespace scimath { namespace lsqrutils {

bool compareGainNames(const std::string& gainA, const std::string& gainB) {
    try {
        std::pair<casa::uInt, std::string> paramInfoA = CalParamNameHelper::extractChannelInfo(gainA);
        std::pair<casa::uInt, std::string> paramInfoB = CalParamNameHelper::extractChannelInfo(gainB);

        // Parameter name excluding channel number.
        std::string parNameA = paramInfoA.second;
        std::string parNameB = paramInfoB.second;

        casa::uInt chanA = paramInfoA.first;
        casa::uInt chanB = paramInfoB.first;

        int res = parNameA.compare(parNameB);

        if (res == 0) {
        // Same names, sort by channel number.
            return (chanA <= chanB);
        } else {
            if (res < 0) {
                return true;
            } else {
                return false;
            }
        }
    }
    catch (AskapError &e) {
        return (gainA.compare(gainB) < 0);
    }
}

void buildLSQRSparseMatrix(const GenericNormalEquations& gne,
                           const std::vector<std::pair<std::string, int> > &indices,
                           lsqr::SparseMatrix &matrix,
                           size_t nParameters,
                           bool matrixIsParallel)
{
    size_t nParametersTotal;
    size_t nParametersSmaller;

#ifdef HAVE_MPI
    MPI_Comm workersComm = matrix.GetComm();
    if (workersComm != MPI_COMM_NULL) {
        int myrank, nbproc;
        MPI_Comm_rank(workersComm, &myrank);
        MPI_Comm_size(workersComm, &nbproc);
        nParametersTotal = lsqr::ParallelTools::get_total_number_elements(nParameters, nbproc, workersComm);
        nParametersSmaller = lsqr::ParallelTools::get_nsmaller(nParameters, myrank, nbproc, workersComm);
    }
    else
#endif
    {
        nParametersTotal = nParameters;
        nParametersSmaller = 0;
    }

    //------------------------------------------------------------------------------------------------------------------------

    if (matrixIsParallel) {
        // Adding starting matrix empty rows, i.e., the rows in a big block-diagonal matrix above the current block.
        for (size_t i = 0; i < nParametersSmaller; ++i) {
            matrix.NewRow();
        }
    }

    if (gne.indexedNormalMatrixInitialized()) {
    // new matrix format
        ASKAPCHECK(gne.indexedDataVectorInitialized(), "Indexed data vector is not initialized!");

        size_t nChannelsLocal = gne.getNumberLocalChannels();
        size_t nBaseParameters = gne.getNumberBaseParameters();

        ASKAPCHECK(2 * nBaseParameters * nChannelsLocal == nParameters, "Wrong number of parameters in buildLSQRSparseMatrix!");

        for (size_t chan = 0; chan < nChannelsLocal; chan++) {
            for (size_t row = 0; row < nBaseParameters; row++) {
                // Adding 2x2 complex elements (indexed with i, j).
                for (size_t i = 0; i < 2; i++) {
                    matrix.NewRow();
                    // Loop over matrix columns.
                    for (size_t col = 0; col < nBaseParameters; col++) {
                        const IndexedNormalMatrix::elem_type& elem = gne.indexedNormalMatrix(col, row, chan);
                        for (size_t j = 0; j < 2; j++) {
                            size_t colIndex = 2 * col + j + chan * (2 * nBaseParameters);
                            matrix.Add(elem.data[i][j], colIndex);
                        }
                    }
                }
            }
        }

    } else {
    // old matrix format

        std::map<std::string, size_t> indicesMap;
        for (std::vector<std::pair<std::string, int> >::const_iterator it = indices.begin();
             it != indices.end(); ++it) {
            indicesMap[it->first] = (size_t)(it->second);
        }

        // Loop over matrix rows.
        for (std::vector<std::pair<std::string, int> >::const_iterator indit1 = indices.begin();
                indit1 != indices.end(); ++indit1) {

            const auto colItBeg = gne.getNormalMatrixRowBegin(indit1->first);
            const auto colItEnd = gne.getNormalMatrixRowEnd(indit1->first);

            if (colItBeg != colItEnd) {
                const size_t nrow = colItBeg->second.nrow();
                for (size_t row = 0; row < nrow; ++row) {
                    matrix.NewRow();
                    // Loop over column elements.
                    for (auto colIt = colItBeg;
                            colIt != colItEnd; ++colIt) {

                        const std::map<std::string, size_t>::const_iterator indicesMapIt = indicesMap.find(colIt->first);
                        if (indicesMapIt != indicesMap.end()) {
                        // It is a parameter to solve for, adding it to the matrix.

                            const size_t colIndex = indicesMapIt->second;
                            const casa::Matrix<double>& nm = colIt->second;

                            ASKAPCHECK(nrow == nm.nrow(), "Not consistent normal matrix element element dimension!");
                            const size_t ncolumn = nm.ncolumn();
                            for (size_t col = 0; col < ncolumn; ++col) {
                                 const double elem = nm(row, col);
                                 ASKAPCHECK(!std::isnan(elem), "Normal matrix seems to have NaN for row = "<< row << " and col = " << col << ", this shouldn't happen!");
                                 matrix.Add(elem, col + colIndex);
                            }
                        }
                    }
                }
            } else {
            // Adding empty matrix rows.
                // Need to add corresponding empty rows in the sparse matrix.
                const size_t nrow = gne.dataVector(indit1->first).nelements();
                for (size_t i = 0; i < nrow; ++i) {
                    matrix.NewRow();
                }
            }
        }
    }

    ASKAPCHECK(matrix.GetCurrentNumberRows() == (nParametersSmaller + nParameters), "Wrong number of matrix rows!");
    if (matrixIsParallel) {
        // Adding ending matrix empty rows, i.e., the rows in a big block-diagonal matrix below the current block.
        size_t nEndRows = nParametersTotal - nParametersSmaller - nParameters;
        for (size_t i = 0; i < nEndRows; ++i) {
            matrix.NewRow();
        }
    }
    ASKAPCHECK(matrix.GetCurrentNumberRows() == nParametersTotal, "Wrong number of matrix rows!");
    matrix.Finalize(nParameters);
}

void getCurrentSolutionVector(const std::vector<std::pair<std::string, int> >& indices,
                              const Params& params,
                              std::vector<double>& solution)
{
    ASKAPCHECK(solution.size() >= 2 * indices.size(), "Wrong size of the solution vector in getCurrentSolutionVector!");

    for (std::vector<std::pair<std::string, int> >::const_iterator indit = indices.begin();
            indit != indices.end(); ++indit) {
        casa::IPosition vecShape(1, params.value(indit->first).nelements());
        casa::Vector<double> value(params.value(indit->first).reform(vecShape));
        for (size_t i = 0; i < value.nelements(); ++i) {
            solution[indit->second + i] = value(i);
        }
    }
}

void getCurrentSolutionVector(const GenericNormalEquations& gne,
                              const Params& params,
                              std::vector<double>& solution)
{
    size_t nChannelsLocal = gne.getNumberLocalChannels();
    size_t nBaseParameters = gne.getNumberBaseParameters();

    for (size_t i = 0; i < nBaseParameters; i++) {
        for (size_t chan = 0; chan < nChannelsLocal; chan++) {
            std::string paramName = gne.getFullParameterName(i, chan);

            auto *data = params.value(paramName).data();
            size_t index = i + nBaseParameters * chan;

            solution[2 * index] = data[0];
            solution[2 * index + 1] = data[1];
        }
    }
}

double getSmoothingWeight(const std::map<std::string, std::string>& parameters,
                          size_t majorLoopIterationNumber) {

    double smoothingWeight = 0.;

    double smoothingMinWeight = solverutils::getParameter("smoothingMinWeight", parameters, 0.);
    double smoothingMaxWeight = solverutils::getParameter("smoothingMaxWeight", parameters, 3.e+6);
    size_t smoothingNsteps = solverutils::getParameter("smoothingNsteps", parameters, 10);

    if (majorLoopIterationNumber < smoothingNsteps) {
        if (smoothingMinWeight == smoothingMaxWeight) {
            smoothingWeight = smoothingMaxWeight;
        } else {
            double span = smoothingMaxWeight - smoothingMinWeight;
            ASKAPCHECK(span > 0, "Wrong smoothing weight!");

            // Logarithmic sweep (between the minimum and maximum weights).
            smoothingWeight = smoothingMinWeight + std::pow(10., log10(span) / (double)(smoothingNsteps) * (double)(majorLoopIterationNumber));
        }
    } else {
        // Relaxation with constant weight.
        smoothingWeight = smoothingMaxWeight;
    }
    return smoothingWeight;
}

bool testMPIRankOrderWithChannels(int workerRank, size_t nChannelsLocal,
                                  const std::vector<std::pair<std::string, int> >& indices)
{
    for (std::vector<std::pair<std::string, int> >::const_iterator indit = indices.begin();
         indit != indices.end(); ++indit) {
        // Extracting channel number from gain name.
        std::string gainName = indit->first;
        std::pair<casa::uInt, std::string> paramInfo = CalParamNameHelper::extractChannelInfo(gainName);
        casa::uInt chan = paramInfo.first;

        // Testing that the channel number is aligned with MPI partitioning:
        // E.g.: for 40 channels and 4 workers, rank 0 has channels 0-9, rank 1: 10-19, rank 2: 20-29, and rank 3: 30-39.
        if (chan < workerRank * nChannelsLocal
            || chan >= (workerRank + 1) * nChannelsLocal) {
            return false;
        }
    }
    return true;
}

// Calculates the index shift for the next channel located on the next MPI rank.
// Assumes channels are ordered with MPI ranks.
size_t getNextChannelIndexShift(size_t nParametersLocal, size_t nChannelsLocal)
{
    return nParametersLocal - (nChannelsLocal - 1) * 2;
}

// Calculates matrix indexes for the Forward Difference (FD) gradient operator.
// For old normal matrix format.
void calculateIndexesFWD_old(size_t nParametersTotal,
                             size_t nParametersLocal,
                             size_t nChannelsLocal,
                             std::vector<int>& leftIndexGlobal,
                             std::vector<int>& rightIndexGlobal)
{
    size_t nextChannelIndexShift = getNextChannelIndexShift(nParametersLocal, nChannelsLocal);

    size_t localChannelNumber = 0;
    for (size_t i = 0; i < nParametersTotal; i += 2) {

        bool lastLocalChannel = (localChannelNumber == nChannelsLocal - 1);

        if (lastLocalChannel) {
            size_t shiftedIndex = i + nextChannelIndexShift;

            if (shiftedIndex < nParametersTotal) {
            // Reached last local channel - shift the 'next' index.
                // Real part.
                leftIndexGlobal[i] = i;
                rightIndexGlobal[i] = shiftedIndex;
                // Imaginary part.
                leftIndexGlobal[i + 1] = leftIndexGlobal[i] + 1;
                rightIndexGlobal[i + 1] = rightIndexGlobal[i] + 1;
            } else {
            // Reached last global channel - do not add constraints - it is already coupled with previous one.
                // Real part.
                leftIndexGlobal[i] = -1;
                rightIndexGlobal[i] = -1;
                // Imaginary part.
                leftIndexGlobal[i + 1] = -1;
                rightIndexGlobal[i + 1] = -1;
            }

        } else {
            // Real part.
            leftIndexGlobal[i] = i;
            rightIndexGlobal[i] = i + 2;
            // Imaginary part.
            leftIndexGlobal[i + 1] = leftIndexGlobal[i] + 1;
            rightIndexGlobal[i + 1] = rightIndexGlobal[i] + 1;
        }

        if (lastLocalChannel) {
            // Reset local channel counter.
            localChannelNumber = 0;
        } else {
            localChannelNumber++;
        }
    }
}

// Calculates matrix indexes for the Forward Difference (FD) gradient operator.
// For new indexed normal matrix format (it has different column order).
void calculateIndexesFWD_new(size_t nParametersTotal,
                             size_t nParametersLocal,
                             size_t nChannelsLocal,
                             std::vector<int>& leftIndexGlobal,
                             std::vector<int>& rightIndexGlobal)
{
    size_t nextChannelIndexShift = nParametersLocal / nChannelsLocal;

    for (size_t i = 0; i < nParametersTotal; i += 2) {
        size_t shiftedIndex = i + nextChannelIndexShift;

        if (shiftedIndex < nParametersTotal) {
            // Real part.
            leftIndexGlobal[i] = i;
            rightIndexGlobal[i] = shiftedIndex;
            // Imaginary part.
            leftIndexGlobal[i + 1] = leftIndexGlobal[i] + 1;
            rightIndexGlobal[i + 1] = rightIndexGlobal[i] + 1;

        } else {
        // Last global channel - do not add constraints - it is already coupled with previous one.
            // Real part.
            leftIndexGlobal[i] = -1;
            rightIndexGlobal[i] = -1;
            // Imaginary part.
            leftIndexGlobal[i + 1] = -1;
            rightIndexGlobal[i + 1] = -1;
        }
    }
}

// Calculates matrix indexes for the Forward Difference (FD) gradient operator.
void calculateIndexesFWD(size_t nParametersTotal,
                         size_t nParametersLocal,
                         size_t nChannelsLocal,
                         std::vector<int>& leftIndexGlobal,
                         std::vector<int>& rightIndexGlobal,
                         bool indexedNormalMatrixFormat)
{
    ASKAPCHECK(nParametersTotal == leftIndexGlobal.size()
               && nParametersTotal == rightIndexGlobal.size(), "Wrong vector size in calculateIndexesFWD!");

    if (indexedNormalMatrixFormat) {
        calculateIndexesFWD_new(nParametersTotal, nParametersLocal, nChannelsLocal, leftIndexGlobal, rightIndexGlobal);
    } else {
        calculateIndexesFWD_old(nParametersTotal, nParametersLocal, nChannelsLocal, leftIndexGlobal, rightIndexGlobal);
    }
}

// Calculates matrix indexes for the Central Difference (CD) gradient operator.
void calculateIndexesCD_old(size_t nParametersTotal,
                            size_t nParametersLocal,
                            size_t nChannelsLocal,
                            std::vector<int>& leftIndexGlobal,
                            std::vector<int>& rightIndexGlobal)
{
    size_t nextChannelIndexShift = getNextChannelIndexShift(nParametersLocal, nChannelsLocal);

    size_t localChannelNumber = 0;
    for (size_t i = 0; i < nParametersTotal; i += 2) {

        bool firstLocalChannel = (localChannelNumber == 0);
        bool lastLocalChannel = (localChannelNumber == nChannelsLocal - 1);

        int shiftedLeftIndex = i - nextChannelIndexShift;
        size_t shiftedRightIndex = i + nextChannelIndexShift;

        if (firstLocalChannel && lastLocalChannel) {
        // One local channel.
            if ((shiftedLeftIndex >= 0) && (shiftedRightIndex < nParametersTotal)) {
                // Real part.
                leftIndexGlobal[i] = shiftedLeftIndex;
                rightIndexGlobal[i] = shiftedRightIndex;
                // Imaginary part.
                leftIndexGlobal[i + 1] = leftIndexGlobal[i] + 1;
                rightIndexGlobal[i + 1] = rightIndexGlobal[i] + 1;

            } else {
            // First/last global channel - do not add constraints - it is already coupled with next/previous one.
                // Real part.
                leftIndexGlobal[i] = -1;
                rightIndexGlobal[i] = -1;
                // Imaginary part.
                leftIndexGlobal[i + 1] = -1;
                rightIndexGlobal[i + 1] = -1;
            }

        } else if (firstLocalChannel) {

            if (shiftedLeftIndex >= 0) {
            // First local channel - shift the 'left' index.
                // Real part.
                leftIndexGlobal[i] = shiftedLeftIndex;
                rightIndexGlobal[i] = i + 2;
                // Imaginary part.
                leftIndexGlobal[i + 1] = leftIndexGlobal[i] + 1;
                rightIndexGlobal[i + 1] = rightIndexGlobal[i] + 1;

            } else {
            // First/last global channel - do not add constraints - it is already coupled with next/previous one.
                // Real part.
                leftIndexGlobal[i] = -1;
                rightIndexGlobal[i] = -1;
                // Imaginary part.
                leftIndexGlobal[i + 1] = -1;
                rightIndexGlobal[i + 1] = -1;
            }
        } else if (lastLocalChannel) {

            if (shiftedRightIndex < nParametersTotal) {
            // Last local channel - shift the 'right' index.
                // Real part.
                leftIndexGlobal[i] = i - 2;
                rightIndexGlobal[i] = shiftedRightIndex;
                // Imaginary part.
                leftIndexGlobal[i + 1] = leftIndexGlobal[i] + 1;
                rightIndexGlobal[i + 1] = rightIndexGlobal[i] + 1;
            } else {
            // Reached last global channel - do not add constraints - it is already coupled with previous one.
                // Real part.
                leftIndexGlobal[i] = -1;
                rightIndexGlobal[i] = -1;
                // Imaginary part.
                leftIndexGlobal[i + 1] = -1;
                rightIndexGlobal[i + 1] = -1;
            }

        } else {
            // Real part.
            leftIndexGlobal[i] = i - 2;
            rightIndexGlobal[i] = i + 2;
            // Imaginary part.
            leftIndexGlobal[i + 1] = leftIndexGlobal[i] + 1;
            rightIndexGlobal[i + 1] = rightIndexGlobal[i] + 1;
        }

        if (lastLocalChannel) {
            // Reset local channel counter.
            localChannelNumber = 0;
        } else {
            localChannelNumber++;
        }
    }
}

// Calculates matrix indexes for the Central Difference (CD) gradient operator.
void calculateIndexesCD_new(size_t nParametersTotal,
                            size_t nParametersLocal,
                            size_t nChannelsLocal,
                            std::vector<int>& leftIndexGlobal,
                            std::vector<int>& rightIndexGlobal,
                            size_t nChannelsShift)
{
    size_t nextChannelIndexShift = nParametersLocal / nChannelsLocal;

    for (size_t i = 0; i < nParametersTotal; i += 2) {

        int shiftedLeftIndex = i - nextChannelIndexShift * nChannelsShift;
        size_t shiftedRightIndex = i + nextChannelIndexShift * nChannelsShift;

        if (shiftedLeftIndex >= 0 && shiftedRightIndex < nParametersTotal) {
            // Real part.
            leftIndexGlobal[i] = shiftedLeftIndex;
            rightIndexGlobal[i] = shiftedRightIndex;
            // Imaginary part.
            leftIndexGlobal[i + 1] = leftIndexGlobal[i] + 1;
            rightIndexGlobal[i + 1] = rightIndexGlobal[i] + 1;

        } else {
        // First or last global channel - do not add constraints - it is already coupled with the next one.
            // Real part.
            leftIndexGlobal[i] = -1;
            rightIndexGlobal[i] = -1;
            // Imaginary part.
            leftIndexGlobal[i + 1] = -1;
            rightIndexGlobal[i + 1] = -1;
        }
    }
}

// Calculates matrix indexes for the Central Difference (CD) gradient operator.
void calculateIndexesCD(size_t nParametersTotal,
                        size_t nParametersLocal,
                        size_t nChannelsLocal,
                        std::vector<int>& leftIndexGlobal,
                        std::vector<int>& rightIndexGlobal,
                        bool indexedNormalMatrixFormat,
                        size_t nChannelsShift = 1)
{
    ASKAPCHECK(nParametersTotal == leftIndexGlobal.size()
               && nParametersTotal == rightIndexGlobal.size(), "Wrong vector size in calculateIndexesCD!");

    if (indexedNormalMatrixFormat) {
        calculateIndexesCD_new(nParametersTotal, nParametersLocal, nChannelsLocal, leftIndexGlobal, rightIndexGlobal, nChannelsShift);
    } else {
        if (nChannelsShift != 1) {
            throw std::invalid_argument("Not supported channels shift!");
        }
        calculateIndexesCD_old(nParametersTotal, nParametersLocal, nChannelsLocal, leftIndexGlobal, rightIndexGlobal);
    }
}

// Calculates matrix middle index for the (high order) Central Difference (CD) gradient operator.
void calculateMiddleIndex(size_t nParametersTotal,
                          const std::vector<int>& leftIndexGlobal,
                          const std::vector<int>& rightIndexGlobal,
                          std::vector<int>& middleIndexGlobal)
{
    for (size_t i = 0; i < nParametersTotal; i++) {
        if (leftIndexGlobal[i] >= 0) {
            assert(rightIndexGlobal[i] >= 0);
            middleIndexGlobal[i] = i;
        } else {
            assert(leftIndexGlobal[i] == -1);
            assert(rightIndexGlobal[i] == -1);
            middleIndexGlobal[i] = -1;
        }
    }
}

// Calculates x[i - delta_i] and x[i + delta_i] indexes, for delta_i = 0, ..., kmax.
void calculateDiagIndexes(size_t nParametersTotal,
                          size_t nParametersLocal,
                          size_t nChannelsLocal,
                          size_t nDiag,
                          std::vector<std::vector<int> >& columnIndexGlobal,
                          bool indexedNormalMatrixFormat,
                          int kmax)
{
    for (size_t k = 0; k < kmax; k++) {
        size_t di = kmax - k;
        int k2 = nDiag - 1 - k;
        // Calculate x[i - di] and x[i + di].
        calculateIndexesCD(nParametersTotal, nParametersLocal, nChannelsLocal, columnIndexGlobal[k], columnIndexGlobal[k2], indexedNormalMatrixFormat, di);
    }
    // Calculate x[i].
    size_t middle_diag = (nDiag - 1) / 2;
    calculateMiddleIndex(nParametersTotal, columnIndexGlobal[0], columnIndexGlobal[nDiag - 1], columnIndexGlobal[middle_diag]);

    // Finalize building the indexes, making them all equal to -1 when the most left/right index is out of bounds.
    for (size_t i = 0; i < nParametersTotal; i++) {
        if (columnIndexGlobal[0][i] < 0) {
            assert(columnIndexGlobal[0][i] == -1);
            assert(columnIndexGlobal[nDiag - 1][i] == -1);
            assert(columnIndexGlobal[middle_diag][i] == -1);

            for (size_t d = 1; d < nDiag - 1; d++) {
                columnIndexGlobal[d][i] = -1;
            }
        }
    }
}

/// @brief Adding smoothness constraints to the system of equations.
void addSmoothnessConstraints(lsqr::SparseMatrix& matrix,
                              lsqr::Vector& b_RHS,
                              const std::vector<double>& x0,
                              size_t nParameters,
                              size_t nChannels,
                              double smoothingWeight,
                              double smoothingLevel,
                              int smoothingType,
                              int smoothingVariableType,
                              int smoothingAccuracy,
                              bool addSpectralDiscont,
                              size_t spectralDiscontStep,
                              bool indexedNormalMatrixFormat,
                              double &cost)
{
    ASKAPCHECK(nChannels > 1, "Wrong number of channels for smoothness constraints!");

    int myrank;
    size_t nParametersTotal;

#ifdef HAVE_MPI
    MPI_Comm workersComm = matrix.GetComm();
    if (workersComm != MPI_COMM_NULL) {
        int nbproc;
        MPI_Comm_rank(workersComm, &myrank);
        MPI_Comm_size(workersComm, &nbproc);
        nParametersTotal = lsqr::ParallelTools::get_total_number_elements(nParameters, nbproc, workersComm);
    }
    else
#endif
    {
        myrank = 0;
        nParametersTotal = nParameters;
    }

    if (myrank == 0) ASKAPLOG_INFO_STR(logger, "Adding smoothness constraints, with smoothingWeight = " << smoothingWeight <<
                                               ", smoothingLevel = " << smoothingLevel <<
                                               ", smoothingType = " << smoothingType <<
                                               ", smoothingVariableType = " << smoothingVariableType <<
                                               ", smoothingAccuracy = " << smoothingAccuracy);

    //-----------------------------------------------------------------------------
    // Assume the same number of channels at every CPU.
    size_t nChannelsLocal = nChannels / (nParametersTotal / nParameters);

    if (myrank == 0) ASKAPLOG_DEBUG_STR(logger, "nChannelsLocal = " << nChannelsLocal);

    //-----------------------------------------------------------------------------
    if (smoothingAccuracy != 2 && smoothingAccuracy != 4 && smoothingAccuracy != 6) {
        throw std::invalid_argument("Non supported accuracy order!");
    }

    size_t nDiag;
    if (smoothingType == 0 || smoothingType == 1) {
        nDiag = 2;
    } else if (smoothingType == 2) {
        nDiag = smoothingAccuracy + 1;
    } else if (smoothingType == 4) {
        nDiag = smoothingAccuracy + 3;
    } else {
        throw std::invalid_argument("Unknown smoothing type!");
    }

    std::vector<std::vector<int> > columnIndexGlobal(nDiag, std::vector<int>(nParametersTotal));
    std::vector<double> kernel(nDiag, 0.);

    if (smoothingType == 0 || smoothingType == 1) {
    // First order.
        auto &leftIndexGlobal = columnIndexGlobal[0];
        auto &rightIndexGlobal = columnIndexGlobal[1];

        if (smoothingType == 0) {
        // Forward difference scheme.
            calculateIndexesFWD(nParametersTotal, nParameters, nChannelsLocal, leftIndexGlobal, rightIndexGlobal, indexedNormalMatrixFormat);
        }
        else if (smoothingType == 1) {
        // Central difference scheme.
            calculateIndexesCD(nParametersTotal, nParameters, nChannelsLocal, leftIndexGlobal, rightIndexGlobal, indexedNormalMatrixFormat);
        }

        kernel[0] = - 1.;
        kernel[1] = 1.;
    }
    else if (smoothingType == 2) {
    // Laplacian.
        if (smoothingAccuracy == 2) {
            calculateDiagIndexes(nParametersTotal, nParameters, nChannelsLocal, nDiag, columnIndexGlobal, indexedNormalMatrixFormat, 1);

            // 1D Laplacian kernel = [1 -2 1].
            kernel[0] = 1.;
            kernel[1] = - 2.;
            kernel[2] = 1.;
        }
        else if (smoothingAccuracy == 4) {
            calculateDiagIndexes(nParametersTotal, nParameters, nChannelsLocal, nDiag, columnIndexGlobal, indexedNormalMatrixFormat, 2);

            // 1D 2nd-order derivative 4th-order accuracy kernel (https://en.wikipedia.org/wiki/Finite_difference_coefficient).
            kernel[0] = -1. / 12.;
            kernel[1] = 4. / 3.;
            kernel[2] = -5. / 2.;
            kernel[3] = kernel[1];
            kernel[4] = kernel[0];
        }
        else if (smoothingAccuracy == 6) {
            calculateDiagIndexes(nParametersTotal, nParameters, nChannelsLocal, nDiag, columnIndexGlobal, indexedNormalMatrixFormat, 3);

            // 1D 2nd-order derivative 6th-order accuracy kernel (https://en.wikipedia.org/wiki/Finite_difference_coefficient).
            kernel[0] = 1. / 90.;
            kernel[1] = -3. / 20.;
            kernel[2] = 3. / 2.;
            kernel[3] = -49. / 18.;
            kernel[4] = kernel[2];
            kernel[5] = kernel[1];
            kernel[6] = kernel[0];
        }
    }
    else if (smoothingType == 4) {
    // Fourth order (bilaplacian).
        if (smoothingAccuracy == 2) {
            calculateDiagIndexes(nParametersTotal, nParameters, nChannelsLocal, nDiag, columnIndexGlobal, indexedNormalMatrixFormat, 2);

            // 1D 4th-order kernel = [1 -4 6 -4 1].
            kernel[0] = 1.;
            kernel[1] = - 4.;
            kernel[2] = 6.;
            kernel[3] = kernel[1];
            kernel[4] = kernel[0];
        }
        else if (smoothingAccuracy == 4) {
            calculateDiagIndexes(nParametersTotal, nParameters, nChannelsLocal, nDiag, columnIndexGlobal, indexedNormalMatrixFormat, 3);

            // 1D 4th-order derivative 4th-order accuracy kernel (https://en.wikipedia.org/wiki/Finite_difference_coefficient).
            kernel[0] = -1. / 6.;
            kernel[1] = 2.;
            kernel[2] = -13. / 2.;
            kernel[3] = 28. / 3.;
            kernel[4] = kernel[2];
            kernel[5] = kernel[1];
            kernel[6] = kernel[0];
        }
        else if (smoothingAccuracy == 6) {
            calculateDiagIndexes(nParametersTotal, nParameters, nChannelsLocal, nDiag, columnIndexGlobal, indexedNormalMatrixFormat, 4);

            // 1D 4th-order derivative 6th-order accuracy kernel (https://en.wikipedia.org/wiki/Finite_difference_coefficient).
            kernel[0] = 7. / 240.;
            kernel[1] = -2. / 5.;
            kernel[2] = 169. / 60.;
            kernel[3] = -122. / 15.;
            kernel[4] = 91. / 8.;
            kernel[5] = kernel[3];
            kernel[6] = kernel[2];
            kernel[7] = kernel[1];
            kernel[8] = kernel[0];
        }
    }

    //---------------------------------------------------------------------------------
    if (smoothingVariableType == 1 || smoothingVariableType == 2) {
    // Apply smoothing to gain magnitude or phase.
        nDiag = 2 * nDiag;

        columnIndexGlobal.resize(nDiag, std::vector<int>(nParametersTotal));
        kernel.resize(nDiag, 0.);
    }

    //---------------------------------------------------------------------------------
    // Note: We could reduce the size of this array to store only local parameters,
    // as this is how it is used anyway in matrix.addParallelSparseOperator().
    std::vector<std::vector<double> > matrixValue(nDiag, std::vector<double>(nParametersTotal));

    // The phase gradient.
    std::vector<double> phaseGradient;

    if (smoothingVariableType == 0) {
    // Apply smoothing to real & imaginary gain parts.
        for (size_t i = 0; i < nParametersTotal; i++) {
            for (size_t k = 0; k < nDiag; k++) {
                // Constant value for every matrix row i.
                matrixValue[k][i] = kernel[k] * smoothingWeight;
            }
        }
    }
    else if (smoothingVariableType == 1 || smoothingVariableType == 2) {
    // Apply smoothing to gain magnitude squared (type = 1) or phase (type = 2),
    // essentially coupling the real and imaginary gain parts together.
    // Note: We utilize the columnIndexGlobal and kernel arrays defined above for the case when smoothing is applied to real and imaginary gain parts,
    // and we redefine these arrays for this type of smoothing.

        if (smoothingVariableType == 2) {
            // Calculate separately the gradient of phase, as we cannot easily express if from its derivatives.
            phaseGradient.resize(nParametersTotal, 0.);
        }

        // Index shift between the real and imaginary parts (diagonals).
        const size_t s = nDiag / 2;

        // Loop over real parts, and add imaginary parts (diagonals).
        for (size_t i = 0; i < nParametersTotal; i += 2) {
            // Filling indexes and kernel for the imaginary part,
            // so that it is on the same row with the real part (coupled).
            // For example: for Laplacian the nDiag = 6, so the real part diagonal indexes are k=0,1,2 and imaginary ones are k=3,4,5.
            for (size_t k = s; k < nDiag; k++) {
                // Take the index from the imaginary parameter (assume it is always next after the real thus i+1 index).
                columnIndexGlobal[k][i] = columnIndexGlobal[k - s][i + 1];
                // Copy the kernel for the imaginary part.
                kernel[k] = kernel[k - s];
            }

            for (size_t k = 0; k < nDiag; k++) {
                if (columnIndexGlobal[k][i] >= 0) {
                    double deriv;
                    if (smoothingVariableType == 1) {
                        // Derivative of (x^2 + y^2) is equal to 2x or 2y.
                        deriv = 2. * x0.at(columnIndexGlobal[k][i]);
                    }
                    else if (smoothingVariableType == 2) {
                        if (k < s) {
                        // x-part
                            double x = x0.at(columnIndexGlobal[k][i]);
                            double y = x0.at(columnIndexGlobal[k + s][i]);
                            // d(phase)/dx
                            deriv = - y / (x * x + y * y);

                            // Calculate the phase gradient (one time per matrix row, hence only for the x-part).
                            double phase = std::atan2(y, x);
                            phaseGradient[i] += kernel[k] * phase;
                        } else {
                        // y-part
                            double y = x0.at(columnIndexGlobal[k][i]);
                            double x = x0.at(columnIndexGlobal[k - s][i]);
                            // d(phase)/dy
                            deriv = x / (x * x + y * y);
                        }
                    }
                    // Setting the matrix value.
                    matrixValue[k][i] = kernel[k] * smoothingWeight * deriv;
                }

                // Skip rows corresponding to imaginary gain parts.
                columnIndexGlobal[k][i + 1] = -1;
            }
        }
    } else {
        throw std::invalid_argument("Unknown smoothing variable type!");
    }

    //-----------------------------------------------------------------------------
    // Adding spectral discontinuities.
    //-----------------------------------------------------------------------------
    if (addSpectralDiscont) {
        if (myrank == 0) ASKAPLOG_INFO_STR(logger, "Adding spectral discontinuities, with step = " << spectralDiscontStep);
        // Number of parameters per channel.
        size_t paramPerChannel = nParametersTotal / nChannels;
        // Skipping constraints every several channels to account for spectral discontinuities.
        for (size_t ch = 0; ch < nChannels - 1; ch++) {
            if ((ch + 1) % spectralDiscontStep == 0) {
                // Loop over all parameters at the current channel.
                for (size_t i = ch * paramPerChannel; i < (ch + 2) * paramPerChannel; i++) {
                    for (size_t j = 0; j < nDiag; j++) {
                        columnIndexGlobal[j][i] = -1;
                    }
                }
            }
        }
    }

    //-----------------------------------------------------------------------------
    // Adding Jacobian of the gradient/Laplacian to the matrix.
    //-----------------------------------------------------------------------------
    matrix.addParallelSparseOperator(nDiag, nParameters, columnIndexGlobal, matrixValue);

    //-----------------------------------------------------------------------------
    // Adding the Right-Hand Side.
    //-----------------------------------------------------------------------------
    size_t b_size0 = b_RHS.size();
    b_RHS.resize(b_RHS.size() + nParametersTotal);
    assert(matrix.GetCurrentNumberRows() == b_RHS.size());

    cost = 0.;
    for (size_t i = 0; i < nParametersTotal; i++) {
        double Ax0 = 0.;
        bool noMatrixElements = true;
        for (size_t k = 0; k < nDiag; k++) {
            if (columnIndexGlobal[k][i] >= 0) {
                noMatrixElements = false;
                Ax0 += matrixValue[k][i] * x0.at(columnIndexGlobal[k][i]);
            }
        }

        double b_RHS_value;
        if (!noMatrixElements) {
            // Setting the right-hand side b = - w (F(x0) - smoothingLevel).
            if (smoothingVariableType == 0) {
                // w F(x0) = A.x0, where F(x) = FreqGrad(x).
                b_RHS_value = - (Ax0 - smoothingWeight * smoothingLevel);
            }
            else if (smoothingVariableType == 1) {
                // w F(x0) = 0.5 A.x0, where F(x) = FreqGrad(x^2 + y^2).
                b_RHS_value = - (0.5 * Ax0 - smoothingWeight * smoothingLevel);
            }
            else if (smoothingVariableType == 2) {
                b_RHS_value = - smoothingWeight * (phaseGradient[i] - smoothingLevel);
            }
        } else {
            // Set zero right-hand side for the empty matrix row.
            b_RHS_value = 0.;
        }

        size_t b_index = b_size0 + i;
        b_RHS[b_index] = b_RHS_value;

        cost += b_RHS_value * b_RHS_value;
    }
    cost = cost / (smoothingWeight * smoothingWeight);
    if (myrank == 0) ASKAPLOG_INFO_STR(logger, "Smoothness constraints cost = " << cost);
}

double calculateCost(const std::vector<double> &b_RHS)
{
    double cost = 0.;
    for (size_t i = 0; i < b_RHS.size(); ++i) {
        cost += b_RHS[i] * b_RHS[i];
    }
    return cost;
}

}}}
